<div class="fluid-container">
	<div class="col-md-6">
		<form class="form-horizontal  max400" method="post" action="<?php echo base_url()?>admin/register_petugas">
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Kategori</label>
					</div>
					<div class="col-md-8">
						<select name="kategori_petugas" class="form-control">
							<?php $data_kategori_petugas = $data_kategori_petugas->result(); ?>
							<?php if (count($data_kategori_petugas) > 0): ?>
								<?php foreach ($data_kategori_petugas as $key2): ?>
									<option value="<?php echo $key2->id_kategori_petugas ?>" <?php if($this->input->post('kategori_petugas') == $key2->id_kategori_petugas) echo "selected" ?>><?php echo $key2->nama_kategori ?></option>		
								<?php endforeach ?>
							<?php endif ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Full Name</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="fullname" placeholder="First Name" class="form-control" value="<?php echo set_value('fullname') ?>" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Username</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="username" placeholder="Username" class="form-control" value="<?php echo set_value('username') ?>"  required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">E-mail</label>
					</div>
					<div class="col-md-8">
						<input type="email" name="email" placeholder="E-mail" class="form-control" value="<?php echo set_value('email') ?>" >
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Password</label>
					</div>
					<div class="col-md-8">
						<input type="password" name="password" placeholder="Password" class="form-control"  required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">reType Password</label>
					</div>
					<div class="col-md-8">
						<input type="password" name="password2" placeholder="reType Password" class="form-control" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Address</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="address" placeholder="Address" class="form-control" value="<?php echo set_value('address') ?>"  required>
					</div>
				</div>
				<button type="submit" name="register" value="1" class="btn btn-success pull-right">Register</button>
			</form>
	</div>
</div>