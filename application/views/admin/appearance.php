<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
			<!-- quick email widget -->
	      	<div class="box box-info">
				<div class="box-header">
					<i class="fa fa-envelope"></i>
					<h3 class="box-title">Web Title</h3>
					<!-- tools box -->
				</div>
				<div class="box-body">
					<form method="post" action="<?php echo base_url() ?>admin/setwebtitle">
						<div class="form-group">
							<input type="webtitle" class="form-control" name="web_title" value="<?php echo $data_appearance->title ?>"><br>
							<button class="pull-right btn btn-default" type="submit" id="sendEmail" name="savetitle">Save Title<i class="fa fa-arrow-circle-right"></i></button>
						</div>
					</form>
				</div>
		    </div>
			<div class="box box-info">
                <div class="box-header">
                	<h3 class="box-title">Deskripsi Website</h3>
                	<!-- tools box -->
                	<div class="pull-right box-tools">
	                    <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
	                </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
	                <form method="post" action="<?php echo base_url() ?>admin/savedeskripsi">
	                    <textarea id="editor1" name="deskripsi" rows="10" cols="80">
                            <?php echo $data_appearance->deskripsi ?>
                    	</textarea><br>
	                    <button class="pull-right btn btn-default" type="submit" id="sendEmail" name="savetitle">Save Deskripsi<i class="fa fa-arrow-circle-right"></i></button>
	                </form>
                </div>
            </div>
            <div class="box box-info">
                <div class="box-header">
                	<h3 class="box-title">FOOTER TEXT  <small>Simple and fast</small></h3>
                	<!-- tools box -->
                	<div class="pull-right box-tools">
	                    <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
	                </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
	                <form method="post" action="<?php echo base_url() ?>admin/savefooter">
	                    <textarea class="textarea" name="footer_content" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"> <?php echo $data_appearance->footer_text ?></textarea><br>
	                    <button class="pull-right btn btn-default" type="submit" id="sendEmail" name="savetitle">Save Footer<i class="fa fa-arrow-circle-right"></i></button>
	                </form>
                </div>
            </div>
		</div>
		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header">
					<i class="fa fa-envelope"></i>
					<h3 class="box-title">Logo</h3>
					<!-- tools box -->
				</div>
				<div class="box-body">
					<img src="<?php echo base_url() ?>assets/img/appstarlogo.png" class="img-responsive">
					<form method="post" action="<?php echo base_url() ?>admin/changelogo" enctype="multipart/form-data">
						<div class="form-group">
							<input type="file" class="form-control" name="userfile" ><br>
							<button class="pull-right btn btn-default" type="submit" id="sendEmail" name="savelogo">Change logo<i class="fa fa-arrow-circle-right"></i></button>
						</div>
					</form>
				</div>
		    </div>
			<div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Color & Time Picker</h3>
                </div>
                <div class="box-body">
                  <!-- Color Picker -->
                  <form method="post" action="<?php echo base_url() ?>admin/changecolor">
                  	<div class="form-group">
	                    <label>Sticky Nav Color</label>
	                    <div class="input-group my-colorpicker2">
	                      <input name="sticky_nav_color" type="text" class="form-control" value="<?php echo $data_appearance->stick_color ?>" required>
	                      <div class="input-group-addon">
	                        <i></i>
	                      </div>
	                    </div><!-- /.input group -->
	                  </div><!-- /.form group -->
	                  <div class="form-group">
	                    <label>Sidebar Menu Color</label>
	                    <div class="input-group my-colorpicker2">
	                      <input name="sidebar_color" type="text" class="form-control" value="<?php echo $data_appearance->sidebar_color ?>" required>
	                      <div class="input-group-addon">
	                        <i></i>
	                      </div>
	                    </div><!-- /.input group -->
	                  </div><!-- /.form group -->
	                  <div class="form-group">
	                    <label>Background Color</label>
	                    <div class="input-group my-colorpicker2">
	                      <input name="background_color" type="text" class="form-control" value="<?php echo $data_appearance->background ?>" required>
	                      <div class="input-group-addon">
	                        <i></i>
	                      </div>
	                    </div><!-- /.input group -->
	                  </div><!-- /.form group -->
	                  <button type="submit" class="pull-right btn btn-default" id="sendEmail" name="savetitle">Save Color<i class="fa fa-arrow-circle-right"></i></button>
                  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
		</div>
	</div>
</div>
