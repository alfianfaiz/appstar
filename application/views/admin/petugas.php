<div class="fluid-container">
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<i class="fa fa-envelope"></i>
				<h3 class="box-title">Cari Petugas</h3>
				<!-- tools box -->
			</div>
			<div class="box-body">
				<form action="#" method="post">
					<div class="form-group">
						<input type="webtitle" class="form-control" name="key_cari_petugas" placeholder="Nama Petugas"><br>
						<div class="btn-group pull-right">
							<button  class="btn btn-default" ><a href="<?php echo base_url() ?>admin/petugas" >Reset<i class="fa fa-arrow-circle-right"></i></a></button>
							<button class=" btn btn-primary" id="sendEmail" name="caripetugas" value="1">Cari<i class="fa fa-arrow-circle-right"></i></button>
						</div>
					</div>
				</form>
			</div>
	    </div>

	    <div class="box box-danger">
			<div class="box-header">
				<i class="fa fa-envelope"></i>
				<h3 class="box-title">Tambah Petugas</h3>
				<!-- tools box -->
			</div>
			<div class="box-body">
				<form action="<?php echo base_url() ?>admin/tambah_petugas" method="post">
					<div class="form-group">
						<label class="control-label">Kategori</label>
						<select name="kategori_petugas" class="form-control">
							<?php $data_kategori_petugas = $data_kategori_petugas->result(); ?>
							<?php if (count($data_kategori_petugas) > 0): ?>
								<?php foreach ($data_kategori_petugas as $key2): ?>
									<option value="<?php echo $key2->id_kategori_petugas ?>"><?php echo $key2->nama_kategori ?></option>		
								<?php endforeach ?>
							<?php endif ?>
						</select><br>
						<button class="pull-right btn btn-success" id="sendEmail" name="savetitle">Tambah Petugas<i class="fa fa-arrow-circle-right"></i></button>
					</div>
				</form>
			</div>
	    </div>
	</div>
	<div class="col-md-8">
		<div class="box box-info">
			<div class="box-header">
				<h4>Daftar Petugas</h4>
			</div>
			<div class="box-body">
				<table class="table table-hover">
					<tr>
						<th>No</th>
						<th>Nama Petugas</th>
						<th>Unit Kerja</th>
						<th>Aksi</th>
					</tr>
					<?php $data_petugas = $data_petugas->result();$no=0; ?>
					<?php if (count($data_petugas)>0): ?>
						<?php foreach ($data_petugas as $key): $no++;?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $key->username ?></td>
								<td><?php echo $key->nama_kategori ?></td>
								<td class="btn-group">
									<button class="btn btn-default btn-sm"><a href="<?php echo base_url() ?>admin/edit_petugas/<?php echo $key->id_user ?>">Edit</a></button>
									<button class="btn btn-default btn-sm"><a href="<?php echo base_url() ?>admin/delete_petugas/<?php echo $key->id_user ?>">Hapus</a></button>
								</td>
							</tr>
						<?php endforeach ?>
					<?php endif ?>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">

	
</script>