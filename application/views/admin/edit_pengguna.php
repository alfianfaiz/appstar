<div class="fluid-container">
	<div class="col-md-6">
		<?php 
			echo validation_errors();
		 ?>
		<form class="form-horizontal  max400" method="post" action="<?php echo base_url()?>admin/update_pengguna">
		<input type="hidden" name="id_user" value="<?php echo $model_pengguna->id_user ?>"></input>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Full Name</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="fullname" placeholder="First Name" class="form-control" value="<?php echo $model_pengguna->fullname ?>" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Username</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="username" placeholder="Username" class="form-control" value="<?php echo $model_pengguna->username ?>"  required>
						<input type="hidden" name="old_username" value="<?php echo $model_pengguna->username ?>"></input>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">E-mail</label>
					</div>
					<div class="col-md-8">
						<input type="email" name="email" placeholder="E-mail" class="form-control" value="<?php echo $model_pengguna->email ?>" required >
						<input type="hidden" name="old_email" value="<?php echo $model_pengguna->email ?>"></input>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Password</label>
					</div>
					<div class="col-md-8">
						<input type="password" name="password" placeholder="Kosongkan jika tidak ingin diganti" class="form-control" >
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">reType Password</label>
					</div>
					<div class="col-md-8">
						<input type="password" name="password2" placeholder="Kosongkan jika tidak ingin diganti" class="form-control" >
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Address</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="address" placeholder="Address" class="form-control" value="<?php echo $model_pengguna->address ?>"  required>
					</div>
				</div>
				<button type="submit" name="register" value="1" class="btn btn-success pull-right">Update</button>
				<a href="<?php echo base_url() ?>admin/user/"><button type="button" class="btn btn-success pull-right">Kembali</button></a>
			</form>
	</div>
</div>