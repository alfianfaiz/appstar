<div class="fluid-container">
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<i class="fa fa-envelope"></i>
				<h3 class="box-title">Web Title</h3>
				<!-- tools box -->
			</div>
			<div class="box-body">
				<form action="#" method="post">
					<div class="form-group">
						<input type="webtitle" class="form-control" name="web-title" placeholder="Title Here"><br>
						<button class="pull-right btn btn-default" id="sendEmail" name="savetitle">Save Title<i class="fa fa-arrow-circle-right"></i></button>
					</div>
				</form>
			</div>
	    </div>
	</div>
	<div class="col-md-8">
		
	</div>
</div>