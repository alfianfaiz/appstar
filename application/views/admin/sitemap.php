<div class="container-fluid">
	<div class="row">
		<div class="col-md-10">
			<div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Halaman Sitemap</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                  <form>
                    <textarea id="editor1" name="editor1" rows="10" cols="80">
                                            This is my textarea to be replaced with CKEditor.
                    </textarea><br>
                    <button type="submit" class="pull-right btn btn-default" id="sendEmail" name="savetitle">Save Sitemap<i class="fa fa-arrow-circle-right"></i></button>
                  </form>
                </div>
              </div><!-- /.box -->
		</div>
	</div>
</div>