<div class="fluid-container">
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<i class="fa fa-envelope"></i>
				<h3 class="box-title">Web Title</h3>
				<!-- tools box -->
			</div>
			<div class="box-body">
				<form action="#" method="post">
					<div class="form-group">
						<input type="webtitle" class="form-control" name="search" placeholder="Title Here"><br>
						<button class="pull-right btn btn-default" id="sendEmail" name="savetitle">Save Title<i class="fa fa-arrow-circle-right"></i></button>
					</div>
				</form>
			</div>
	    </div>
	</div>
	<div class="col-md-8">
		<div class="box box-info">
			<div class="box-header">
				Nama Pengguna
			</div>
			<div class="box-body">
				<table class="table">
					<tr>
						<td>No</td>
						<td>Username</td>
						<td>Email</td>
						<td>Registration Date</td>
						<td>Action</td>
					</tr>
					<?php if ($data_search != null): $no = 0; ?>
						<?php foreach ($data_search as $key): $no++ ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $key->username ?></td>
								<td><?php echo $key->email ?></td>
								<td><?php echo date('d-M-Y',strtotime($key->create_date)) ?></td>
								<td>
									<button class="btn btn-default btn-sm"><a href="<?php echo base_url() ?>admin/edit_pengguna/<?php echo $key->id_user ?>">Edit</a></button>
									<button class="btn btn-default btn-sm"><a href="<?php echo base_url() ?>admin/deletePengguna/<?php echo $key->id_user ?>">Hapus</a></button>
								</td>
							</tr>
						<?php endforeach ?>
					<?php endif ?>
				</table>
			</div>
		</div>
	</div>
</div>