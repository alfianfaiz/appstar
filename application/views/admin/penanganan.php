
<div class="container-fluid">
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<i class="fa fa-envelope"></i>
				<h3 class="box-title">Kategori Masalah</h3>
				<!-- tools box -->
			</div>
			<div class="box-body">
				<form method="post" action="#">
					<div class="form-group">
						<select class="form-control" name="kategori_post" id="kategori_post">
							<?php $data_kategori_post = $data_kategori_post->result(); ?>
							<?php if (count($data_kategori_post) > 0): ?>
								<?php foreach ($data_kategori_post as $key2): ?>
									<option value="<?php echo $key2->id_kategori_post ?>" <?php  if($key2->id_kategori_post == $id_kategori_post) echo "selected"; ?>><?php echo $key2->nama_kategori ?></option>
								<?php endforeach ?>
							<?php endif ?>
						</select><br>
						<div class="btn-group pull-right">
							<a class="btn btn-default" href="<?php echo base_url() ?>admin/kelolapenanganan" >Reset<i class="fa fa-arrow-circle-right"></i></a>
							<button type="submit" class="btn btn-default">Tampilkan<i class="fa fa-arrow-circle-right"></i></button>
						</div>
					</div>
				</form>
			</div>
	    </div>

	    <div class="box box-info">
			<div class="box-header">
				<i class="fa fa-envelope"></i>
				<h3 class="box-title">Kategori Petugas Tersedia</h3>
				<!-- tools box -->
			</div>
			<div class="box-body">
				<form method="post" action="<?php echo  base_url() ?>admin/addpetugaspenagan">
				<input type="hidden" name="nama_kategori_post" value="<?php echo $pencarian ?>"></input>
					<div class="form-group">
						<select class="form-control" name="kategori_petugas" required>
							<?php $data_kategori_petugas = $data_kategori_petugas->result(); ?>
							<?php if ($pencarian != "Tidak Ada" && count($data_kategori_petugas) > 0): ?>
								<?php foreach ($data_kategori_petugas as $key2): ?>
									<option value="<?php echo $key2->id_kategori_petugas ?>"><?php echo $key2->nama_kategori ?></option>		
								<?php endforeach ?>
							<?php endif ?>
						</select><br>
						<button type="submit" class="pull-right btn btn-default" id="sendEmail" name="savetitle">Tambah ke Penangan<i class="fa fa-arrow-circle-right"></i></button>
					</div>
				</form>
			</div>
	    </div>
	</div>
	<div class="col-md-8">
		<div class="box box-info">
			<div class="box-header">
				<i class="fa fa-envelope"></i>
				<h3 class="box-title">Kategori Petugas Menagani Masalah <b id="keyword">"<?php echo $pencarian ?>"</b></h3>
				<!-- tools box -->
			</div>
			<div class="box-body">
				<table class="table">
					<tr>
						<td>No.</td>
						<td>Nama Kategori Petugas</td>
						<td>Aksi</td>
					</tr>
					<tbody id="data_petugas">
						<?php $data_petugas_post = $data_petugas_post->result(); $no=0;?>
						<?php if ($pencarian != "Tidak Ada" && count($data_petugas_post) > 0): ?>
							<?php foreach ($data_petugas_post as $key): $no++?>
								<tr>
									<td><?php echo $no ?></td>
									<td><?php echo $key->nama_kategori ?></td>
									<td><button class="btn btn-default btn-sm"><a href="">Hapus</a></button></td>
								</tr>	
							<?php endforeach ?>
						<?php endif ?>
					</tbody>
				</table>
			</div>
	    </div>
	</div>
</div>