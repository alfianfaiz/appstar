<div class="container-fluid">
	<div class="row">
		<div class="col-md-10">
			<div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Halaman Tentang</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <div class="box-body pad">
                  <form method="post" action="<?php echo base_url() ?>admin/simpantentang">
                    <textarea id="editor1" name="tentang_content" rows="10" cols="80">
                          <?php echo $data_appearance->tentang ?>
                    </textarea><br>
                    <button type="submit" class="btn  btn-info">Simpan Text Tentang</button>
                  </form>
                </div>
              </div><!-- /.box -->
		</div>
	</div>
</div>