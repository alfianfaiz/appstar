<?php 
$base_theme = base_url()."/assets/themes/universal/";
$base_assets = base_url()."/assets/";
$data = array(
    'base_theme' => $base_theme,
    'base_assets' => $base_assets,
    );
 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Universal - All In 1 Template</title>

    <meta name="keywords" content="">

    <!-- <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'> -->

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="<?php echo $base_theme; ?>css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo $base_theme; ?>css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="<?php echo $base_theme; ?>css/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="<?php echo $base_theme; ?>css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="<?php echo $base_theme; ?>css/custom.css" rel="stylesheet">

    <!-- Responsivity for older IE -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="<?php echo $base_theme; ?>img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php echo $base_theme; ?>img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $base_theme; ?>img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $base_theme; ?>img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $base_theme; ?>img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $base_theme; ?>img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $base_theme; ?>img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $base_theme; ?>img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $base_theme; ?>img/apple-touch-icon-152x152.png" />
    <!-- owl carousel css -->

    <link href="<?php echo $base_theme; ?>css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo $base_theme; ?>css/owl.theme.css" rel="stylesheet">
</head>

<body>
    <header>
            <div class="top-nav-sticky">
                <div class="row">
                    <?php $this->load->view('web/pages/partial/_sticky_nav',$data) ; ?>
                    <div class="col-xs-12 col-sm-12 col-md-12 second-nav">
                        <?php 
                            $data2 = array('title'=>$title);
                            $this->load->view('web/pages/partial/_page_title',$data2) ;
                        ?>
                    </div>
                    <div class="visible-xs col-xs-12 collapse-langganan">
                        <h4>Langganan</h4>
                        <ul class="langganan-box">
                            <li>
                                <a href="">
                                    <span class="langganan_thumb">
                                        <img src="<?php echo $base_theme ?>/img/person-4.jpg" width="19px">
                                    </span>
                                    <span class="langganan_name">AdeleVEVO</span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="langganan_thumb">
                                        <img src="<?php echo $base_theme ?>/img/person-4.jpg" width="19px">
                                    </span>
                                    <span class="langganan_name">KatyPerryVEVO</span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="langganan_thumb">
                                        <img src="<?php echo $base_theme ?>/img/person-4.jpg" width="19px">
                                    </span>
                                    <span class="langganan_name">Yufid.TV ...</span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="langganan_thumb">
                                        <img src="<?php echo $base_theme ?>/img/person-4.jpg" width="19px">
                                    </span>
                                    <span class="langganan_name">Langganan 1</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
    </header>
    echo
    <div class="container-fluid">
        <div class="row all-container">
            <div class="col-md-10 col-md-offset-1 main-content">
                <?php if (isset($page)): ?>
                    <?php $this->load->view('web/pages/'.$page,$data) ?>
                <?php else: ?>
                    
                <?php endif ?>
            </div>
            <div class="col-md-10 col-md-offset-1 nopadding">
                <footer>
                    <div class="top-footer" style="border-bottom: 1px solid #ccc">
                        <img src="<?php echo $base_assets ?>img/appstarlogo.png" height="38px;">
                        <a href="">ALL</a>
                        <a href="">HEADLINE</a>
                        <a href="">LATEST PROBLEM</a>
                    </div>
                    <div class="bottom-footer" style="padding-top: 7px;">
                        <a href="">Tentang AppStar</a>
                        <a href="">Site Map</a>
                        <a href="">Profile</a>
                        <a href="">Developer</a>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    
    

    <!-- #### JAVASCRIPT FILES ### -->

    <script src="<?php echo $base_theme ?>/js/jquery-2.1.4.js"></script>
    <script>
        window.jQuery || document.write('<script src="<?php echo $base_theme; ?>js/jquery-1.11.0.min.js"><\/script>')
    </script>
    <script src="<?php echo $base_theme; ?>js/bootstrap.min.js"></script>

    <script src="<?php echo $base_theme; ?>js/jquery.cookie.js"></script>
    <script src="<?php echo $base_theme; ?>js/waypoints.min.js"></script>
    <script src="<?php echo $base_theme; ?>js/jquery.counterup.min.js"></script>
    <script src="<?php echo $base_theme; ?>js/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo $base_theme; ?>js/front.js"></script>

    

    <!-- owl carousel -->
    <script src="<?php echo $base_theme; ?>js/owl.carousel.min.js"></script>



</body>

</html>