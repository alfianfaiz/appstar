<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-8">
		<div class="default-wrapper big-padding">
			<h2>Welcome to Appstar</h2>
			<p>Fill details below to register</p>
			<div class="container-fluid">
				<div>
					<?php echo validation_errors(); ?>
				</div>
			<form class="form-horizontal  max400" method="post" action="<?php echo base_url()?>login/registerme">
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Full Name</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="fullname" placeholder="First Name" class="form-control" value="<?php echo set_value('fullname') ?>" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Username</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="username" placeholder="Username" class="form-control" value="<?php echo set_value('username') ?>"  required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">E-mail</label>
					</div>
					<div class="col-md-8">
						<input type="email" name="email" placeholder="E-mail" class="form-control" value="<?php echo set_value('email') ?>" >
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Password</label>
					</div>
					<div class="col-md-8">
						<input type="password" name="password" placeholder="Password" class="form-control"  required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">reType Password</label>
					</div>
					<div class="col-md-8">
						<input type="password" name="password2" placeholder="reType Password" class="form-control" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-4">
						<label class="control-label">Address</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="address" placeholder="Address" class="form-control" value="<?php echo set_value('address') ?>"  required>
					</div>
				</div>
				<button type="submit" name="register" value="1" class="btn btn-success pull-right">Register</button>
			</form>
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm- col-md-4">
		<div class="default-wrapper">
			<div class="default-wrapper blue-banner big-padding register-banner">
				<center>
					<img src="<?php echo base_url() ?>assets/img/white-logo.png" class="img-responsive" width="200px">
					<br>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
				</center>
			</div>
			<div class="default-wrapper normal-padding">
				<h3>Already Have an Account ?</h3>
				<form style="max-width:300px;" class="container-fluid" method="post" action="<?php echo base_url() ?>login/loginme">
					<div class="form-group">
						<input type="text" name="email" placeholder="Email" class="form-control" required>
					</div>
					<div class="form-group">
						<input type="password" name="password" placeholder="Password" class="form-control" required>
					</div>
					<div class="col-md-6">
						<button type="submit" class="btn btn-primary">Login Me !</button> 
					</div>
					<div class="col-md-1" style="line-height: 2.2em">
						Or
					</div>
					<div class="col-md-4 social-register">
						<i class="fa fa-facebook-square"></i>
						<i class="fa fa-twitter-square"></i>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>