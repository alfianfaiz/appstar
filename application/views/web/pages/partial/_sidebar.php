<div class="col-md-2 sidebar-left hidden-xs hidden-sm">
                <div class="common-menu hidden" >
                    <h3>Welcome to AppStar</h3>
                    <ul>
                       <li><a href="">HEADLINE</a></li> 
                       <li><a href="">LATEST PROBLEM</a></li>
                       <li><a href="<?php echo base_url(); ?>login">LOGIN / SIGNUP</a></li>
                    </ul>
                </div>
                <div class="user-data ">
                <?php 
                    if ($this->session->userdata('avatar') == "default-avatar.png") {
                        $avatar = $this->session->userdata('avatar');
                    } else $avatar = $this->session->userdata('avatar');
                 ?>
                    <div class="row ">
                        <div class="col-md-12 nopadding top-user-img">
                            <img src="<?php echo base_url() ?>uploads/avatar/<?php echo $avatar?>" width="100%">
                        </div>
                        <div class="col-md-12">
                            <div class="container-fluid">
                                <img src="<?php echo base_url() ?>uploads/avatar/<?php echo $avatar?>" class="img-responsive img-circle little-img" width="65px">
                                <h2 class="user-data-name"><a href="<?php echo base_url() ?>app/profile/<?php echo  $this->session->userdata('username') ?>"><?php echo $this->session->userdata('username') ?></a></h2>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2" style="padding-left: 10px">
                                    <a>Posts</a>
                                    <score><?php echo $base_model_pengguna->getProblemPosted(); ?></score>
                                </div>
                                <div class="col-md-4">
                                    <a>Langganan</a>
                                    <score><?php echo $base_model_pengguna->getUserBerlangganan(); ?></score>
                                </div>
                                <div class="col-md-3">
                                    <a>Berlangganan</a>
                                    <score><?php echo $base_model_pengguna->getUserPelanggan(); ?></score>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="button_post">
                    <a href="<?php echo base_url() ?>app/posting_problem" class="form-control btn btn-primary" style="font-family: 'Roboto' !important">
                        <i class="fa fa-star"></i> Post New Problem
                    </a>
                </div>
                <hr>
                <div class="sidebar-content langganan-box">
                    <h4>Langganan</h4>
                    <ul class="langganan-box">
                        <?php 
                            $queryLangganan = $base_model_langganan->getBerlangganan($this->session->userdata('id_user'));
                            $queryLangganan = $queryLangganan->result();
                            $cek = count($queryLangganan);
                         ?>
                         <?php foreach ($queryLangganan as $key ): ?>
                        <li>
                            <a href="<?php echo base_url() ?>app/profile/<?php echo $key->username ?>">
                                <span class="langganan_thumb">
                                    <img src="<?php echo base_url() ?>uploads/avatar/<?php echo $key->avatar ?>" width="19px">
                                </span>
                                <span class="langganan_name"><?php echo $key->username ?></span>
                            </a>
                        </li>     
                         <?php endforeach ?>
                        <?php if ($cek == 0): ?>
                            <?php $queryRandom = $base_model_pengguna->getRandomPelanggan() ?>              
                            <?php foreach ($queryRandom->result() as $key): ?>
                                <li>
                                    <a href="<?php echo base_url() ?>app/profile/<?php echo $key->username ?>">
                                        <span class="langganan_thumb">
                                            <img src="<?php echo base_url() ?>uploads/avatar/<?php echo $key->avatar ?>" width="19px">
                                        </span>
                                        <span class="langganan_name"><?php echo $key->username ?></span>
                                    </a>
                                </li> 
                            <?php endforeach ?>
                        <?php endif ?>                        
                    </ul>
                </div>
            </div>