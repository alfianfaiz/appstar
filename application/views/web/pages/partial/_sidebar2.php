<div class="col-md-2 sidebar-left hidden-xs hidden-sm">
                <div class="common-menu hidden" >
                    <h3>Welcome to AppStar</h3>
                    <ul>
                       <li><a href="">HEADLINE</a></li> 
                       <li><a href="">LATEST PROBLEM</a></li>
                       <li><a href="<?php echo base_url(); ?>login">LOGIN / SIGNUP</a></li>
                    </ul>
                </div>
                <div class="user-data ">
                <?php 
                    if ($this->session->userdata('avatar') == "default-avatar.png") {
                        $avatar = "default-avatar.png";
                    } else $avatar = $this->session->userdata('avatar');
                 ?>
                    <div class="row ">
                        <div class="col-md-12 nopadding top-user-img">
                            <img src="<?php echo base_url() ?>uploads/avatar/<?php echo $avatar?>" width="100%">
                        </div>
                        <div class="col-md-12">
                            <div class="container-fluid">
                                <img src="<?php echo base_url() ?>uploads/avatar/<?php echo $avatar?>" class="img-responsive img-circle little-img" width="65px">
                                <h2 class="user-data-name"><a href="<?php echo base_url() ?>app/profile/<?php echo  $this->session->userdata('username') ?>"><?php echo $this->session->userdata('username') ?></a></h2>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4" style="padding-left: 10px">
                                    <a>Ditangani</a>
                                    <score><?php echo count($base_model_pengguna->getAllPostDitanganiForPetugas($base_model_pengguna->id_user)); ?></score>
                                </div>
                                <div class="col-md-6" style="padding-left: 10px">
                                    <a>Belum Ditangani</a>
                                    <score><?php echo count($base_model_pengguna->getAllPostBelumDitanganiForPetugas($base_model_pengguna->id_user)); ?></score>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <?php if ($this->session->userdata('level') == 3): ?>
                    <div class="button_post">
                    <a href="<?php echo base_url() ?>app/posting_problem" class="form-control btn btn-primary" style="font-family: 'Roboto' !important">
                        <i class="fa fa-star"></i> Post New Problem
                    </a>
                </div>
                <?php endif ?>
                <hr>
                <div class="sidebar-content langganan-box">
                    <h4>Kategori Kesalahan</h4>
                    <ul class="langganan-box">
                        <?php 
                            $query_kategori_post = $base_model_pengguna->getKategoriDitangani($this->session->userdata('kategori_petugas_id'));
                            $cek = count($query_kategori_post);
                         ?>
                         <?php foreach ($query_kategori_post as $key ): ?>
                        <li>
                            <a href="<?php echo base_url() ?>app2/index/<?php echo underscore($key->nama_kategori) ?>">
                                <span class="glyphicon glyphicon-bullhorn"></span>
                                <span class="langganan_name"><?php echo $key->nama_kategori ?></span>
                            </a>
                        </li>     
                         <?php endforeach ?>
                        <?php if ($cek == 0): ?>
                            <?php $queryRandom = $base_model_pengguna->getRandomPelanggan() ?>              
                            <?php foreach ($queryRandom->result() as $key): ?>
                                <li>
                                    <a href="<?php echo base_url() ?>app/profile/<?php echo $key->username ?>">
                                        <span class="langganan_thumb">
                                            <img src="<?php echo base_url() ?>uploads/avatar/<?php echo $key->avatar ?>" width="19px">
                                        </span>
                                        <span class="langganan_name"><?php echo $key->username ?></span>
                                    </a>
                                </li> 
                            <?php endforeach ?>
                        <?php endif ?>                        
                    </ul>
                </div>
            </div>