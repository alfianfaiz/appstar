                    <div class="col-xs-12 col-sm-2 col-md-2">
                        <a href="<?php echo base_url() ?>"><img src="<?php echo $base_assets ?>img/appstarlogo.png" class="logo"></a>
                    </div>
                    <div class=" col-sm-offset-1 hidden-xs col-sm-6 col-md-6 col-md-offset-0 search-form">
                        <div class="row">
                        <?php 
                            if ($this->session->userdata('level')==2) {
                                $search_url = "app2/search_problem";
                            } else $search_url = "app/search_problem";
                         ?>
                            <form action="<?php echo base_url().$search_url ?>" method="post">
                                <div class="col-xs-9 col-md-10 nopadding">
                                    <input type="text" class="form-control" name="search" style="height: 30px;" required>
                                </div>
                                <div class=" col-xs-2 col-md-1 nopadding">
                                    <button type="submit" class="btn btn-default form-control input-sm"><i class="fa fa-search" style="padding: 0px;"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xs-12 visible-xs">
                        <div class="row">
                        <center>
                            <form>
                                <div class="col-xs-6 col-xs-offset-2">
                                    <input type="text" class="form-control" name="search" placeholder="search" style="height: 30px;" required>
                                </div>
                                <div class="col-xs-2">
                                    <button type="submit" class="btn btn-default form-control input-sm"><i class="fa fa-search" style="padding: 0px;"></i></button>
                                </div>
                            </form>
                            </center>
                        </div>
                    </div>
                    <div class=" col-xs-6 col-xs-offset-3 col-sm-3 col-sm-offset-0 col-md-2 col-md-offset-2  top-shortcut">
                        <center>
                            
                                
                            <?php if ($this->session->userdata('IS_LOGGED_IN')): ?>
                                <?php if ($this->session->userdata('level')==3): ?>
                                <a href="<?php echo base_url() ?>app/search_location" alt="Search Location"><i class="fa fa-map-marker"></i></a>
                                <div class="dropdown">
                                  <a href="#" alt="Notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-bell"></i></a>
                                  <!-- </button> -->
                                  <ul class="dropdown-menu dropdown-me" aria-labelledby="dropdownMenu1">
                                    <?php 
                                        $model_pengguna_notif = new Pengguna();
                                        $query_notif = $model_pengguna_notif->getNotifikasi($this->session->userdata('id_user'),9);
                                     ?>
                                     <?php foreach ($query_notif->result() as $key): ?>
                                        <?php if ($key->caption == "null"): ?>
                                            <?php 
                                                $this->db->where('id_user',$key->id_post);
                                                $this->db->select('username');
                                                $query = $this->db->get('pengguna')->result();
                                                $query = $query[0]->username;
                                             ?>
                                            <li><a href="<?php echo base_url(); ?>app/profile/<?php echo $query ?>"><?php echo $query." ".$key->pesan ?></a></li>
                                        <?php else: ?>
                                            <li><a href="<?php echo base_url(); ?>app/problem/<?php echo $key->id_post ?>"><?php echo $key->username." ".$key->pesan ?></a></li>
                                        <?php endif ?>
                                     <?php endforeach ?>
                                  </ul>
                                </div>
                                <div class="dropdown">
                                  <a  alt="Profile" class="hidden-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><img src="<?php echo base_url() ?>uploads/avatar/<?php echo $this->session->userdata('avatar')?>" class="img-responsive img-circle"></a>
                                    <a href="#" alt="Profile" class="visible-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-user"></i></a>
                                  <ul class="dropdown-menu" style="width:90px;left:0px" aria-labelledby="dropdownMenu1">
                                    <li><a href="<?php echo base_url() ?>app/profile/<?php echo $this->session->userdata('username') ?>"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
                                    <li><a href="<?php echo base_url() ?>setting"><span class="glyphicon glyphicon-cog"></span> Setting</a></li>
                                    <li><a href="<?php echo base_url() ?>login/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                                  </ul>
                                </div>
                            <?php else: ?>
                                <a href="<?php echo base_url() ?>setting" alt="Setting"><span class="glyphicon glyphicon-cog"></span></a>
                                <a href="<?php echo base_url() ?>login/logout"><span class="glyphicon glyphicon-log-out"></span></a>
                                <a alt="Profile" class="hidden-xs"><img src="<?php echo base_url() ?>uploads/avatar/<?php echo $this->session->userdata('avatar');?>" class="img-responsive img-circle"></a>
                                    <a href="#" alt="Profile" class="visible-xs"><i class="fa fa-user"></i></a>
                            <?php endif ?>
                            <?php else: ?>
                                <a href="<?php echo base_url() ?>app/login" alt="login" class="btn btn-sm btn-primary">Login</a>
                            <?php endif ?>
                            
                            
                        </center>
                    </div>
                    <div class="visible-xs  col-xs-8 col-xs-offset-3">
                        <span class=""><button class="btn btn-primary">Create New Post</button></span>
                        <span class=""><a class="btn btn-primary"><i class="glyphicon glyphicon-th"></i></a></span>
                    </div>