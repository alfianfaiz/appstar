<?php 
	$this->load->model('Post') ;
	$model_post_sidebar = new Post();
	$queryLatest = $model_post_sidebar->getLatestProblem(5);
	$tool = new Tool();
?>
<div class="default-wrapper normal-padding">
			<h4>LATEST PROBLEM</h4>
			<div class="container-fluid">
				<?php foreach ($queryLatest->result() as $key): ?>
					<div class="media">
						<div class="media-left">
							<div class="fullwidth-image" style="width:140px">
								<center>
									<img src="<?php echo base_url() ?>uploads/post/<?php echo $key->image ?>" height="100px" class="media-object">
								</center>
							</div>
						</div>
						<div class="media-body">
							<a href="<?php echo base_url() ?>app/problem/<?php echo $key->id_post ?>"><h4 class="media-heading">Heading</h4></a>
							<a href="<?php echo base_url() ?>app/problem/<?php echo $key->id_post ?>" style="color: #000"><?php echo $tool->generateExcerpt($key->caption,170) ?>
							</a>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>