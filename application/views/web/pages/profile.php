<div class="row">
	<div class="col-md-10 ">
		<div class="row">
			<div class="col-md-3" >
				<div class="thumbnail-me pull-right">
					<img src="<?php echo base_url() ?>uploads/avatar/<?php echo $model_pengguna->avatar ?>" class="img-responsive img-circle ">
				</div>
			</div>
			<div class="col-md-5 profile-info">
				<div class="col-md-12">
					<h2 class="profile-info-name"><?php echo $model_pengguna->username ?></h2>
				</div>
				<div class="col-md-3 profile-info-sub">
					<p>Masalah</p>
					<h3><?php echo $model_pengguna->getProblemPosted() ?></h3>
				</div>
				<div class="col-md-4 profile-info-sub">
					<p>Langganan</p>
					<h3><?php echo $model_pengguna->getUserBerlangganan() ?></h3>
				</div>
				<div class="col-md-4 profile-info-sub">
					<p>Berlangganan</p>
					<h3><?php echo $model_pengguna->getUserPelanggan() ?></h3>
				</div>
			</div>
			<?php if ($model_pengguna->id_user == $this->session->userdata('id_user')): ?>
				<div class="col-md-1 profile-info-buttonsunting">
					<a href="<?php echo base_url() ?>setting" class="btn btn-default">Sunting Profil</a>
				</div>	
			<?php else: ?>
				<div class="col-md-1 profile-info-buttonsunting">
					<?php if ($model_langganan->checkLangganan($this->session->userdata('id_user'),$model_pengguna->id_user) > 0): ?>
						<a href="<?php echo base_url() ?>app/berlangganan/<?php echo $model_pengguna->username ?>" class="btn btn-success">
						Berhenti berlangganan</a>
					<?php else: ?>
						<a href="<?php echo base_url() ?>app/berlangganan/<?php echo $model_pengguna->username ?>" class="btn btn-danger">
						Berlangganan</a>
					<?php endif ?>
					
				</div>	
			<?php endif ?>
		</div>
		<div class="default-wrapper normal-padding" style="margin-top: 20px;">
			<div class="container-fluid">
			<?php $arrayResult = $model_pengguna->getUserPost()->result() ?>
				<?php if (count($arrayResult) == 0): ?>
					<h2>Belum Memposting</h2>
				<?php endif ?>
				<?php foreach ( $arrayResult as $key): ?>
					<div class="col-xs-12 col-sm-3 col-md-3 item-box">		
		                <div class="fullwidth-image">
		                    <center>
		                    	<img src="<?php echo base_url() ?>uploads/post/<?php echo $key->image ?>" height="145px">
		                    </center>
		                </div>
		                <h6><a href="<?php echo base_url() ?>app/problem/<?php echo $key->id_post ?>"><?php echo $key->caption ?></a></h6>
		                <p class="box-summary-comment"><?php echo $model_post->countCommentById($key->id_post) ?> Komentar</p>
		            </div>	
				<?php endforeach ?>
				
			</div>
		</div>
	</div>
</div>