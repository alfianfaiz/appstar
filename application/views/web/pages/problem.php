
<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7">
		<div class="default-wrapper">
			<div class="fullwidth-image">
				<center>
					<img src="<?php echo base_url() ?>uploads/post/<?php echo $data_post->image ?>" height="400px" width="100%">
				</center>
			</div>
			<div class="container-fluid"> 
				<div class="">
					<div class="col-md-10 normal-padding">
						<h4><?php echo $data_post->title ?></h4>
						<div class="image" style="float:left;margin-right: 7px;">
							<img src="<?php echo base_url() ?>uploads/avatar/<?php echo $this->session->userdata('avatar') ?>" class="" width="50px">	
						</div>
						<div class="" style="float:left;margin-right: 7px;">
							<h5 class="nomargin" style="font-weight: lighter;font-size: 1.2em;line-height: 1.2em;"><?php echo  $data_post->username ?></h5>
							<?php if ($this->session->userdata('id_user') != $data_post->id_pengguna): ?>
								<?php if ($this->Langganan->checkLangganan($this->session->userdata('id_user'),$data_post->id_pengguna) > 0): ?>
								<a href="<?php echo base_url() ?>app/berlangganan/<?php echo $data_post->username ?>" class="btn btn-success btn-sm">
								Berhenti berlangganan</a>
							<?php else: ?>
								<a href="<?php echo base_url() ?>app/berlangganan/<?php echo $data_post->username ?>" class="btn btn-danger btn-sm">
								Berlangganan</a>
							<?php endif ?>
							<?php endif ?>
						</div>
					</div>
					<div class="col-md-2 normal-padding">
					<br>
						<table>
							<tr>
								<td align="center"> <h2 class="nopadding nomargin"><?php echo  $model_post->countBantuan() ?></h2>Bantuan</td>
							</tr>
							<tr>
								<td><button class="btn btn-warning" data-toggle="modal" data-target="#myModal">Bantu !</button></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="default-wrapper normal-padding">
			Diterbitkan Pada : <?php echo $data_post->create_date ?>
			<br>
			<i class="fa fa-map-marker" style="color:red"></i> <b><?php echo $data_post->location_name ?></b>
			<p><?php echo $data_post->caption; ?></p>
		</div>
		<div class="default-wrapper normal-padding">
			<ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#komentar" aria-controls="home" role="tab" data-toggle="tab">Komentar</a></li>
			    <li role="presentation"><a href="#bantuan" aria-controls="profile" role="tab" data-toggle="tab">Bantuan</a></li>
			  </ul>
			   <!-- Tab panes -->
			  <div class="tab-content">
			    <div role="tabpanel" class="tab-pane active" id="komentar">
			    	<?php $query_komentar = $model_komentar->getCommentAndHelpList($model_post->id_post) ?>
			    	<div class="comment-item">
			    		<div class="media">
							<div class="media-left">
								<div class="fullwidth-image" style="width:60px">
									<center>
										<img src="<?php echo base_url() ?>uploads/avatar/default-avatar.png" height="60px" class="media-object">
									</center>
								</div>
							</div>
							<div class="media-body">
								<h4 class="media-heading">Post Comment</h4>
								<form method="post" action="<?php echo base_url() ?>app/postHelp">
									<textarea name="post_content" class="form-control" placeholder="write comments"></textarea>
									<input type="hidden" name="id_post" value="<?php echo $model_post->id_post ?>"></input>
									<button type="submit" class="btn btn-primary pull-right" name="kategori" value="1">Poskan</button>
								</form>
							</div>
						</div>
			    	</div>
					<?php foreach ($query_komentar->result() as $key): ?>
						<div class="comment-item" <?php if ($key->kategori ==2): ?>
							style="background-color: #FFFAC2"
						<?php endif ?>>

						<div class="container-fluid">
							<?php if ($key->avatar == "default"): ?>
								<div class="comment-image"><img src="<?php echo base_url() ?>uploads/avatar/default-avatar.png" width="50px"></div>
							<?php else: ?>
								<div class="comment-image"><img src="<?php echo base_url() ?>uploads/avatar/<?php echo $key->avatar ?>" width="50px"></div>
							<?php endif ?>
							<div class="comment-text">
								<b> <?php echo $key->username ?></b> 
								<i> <?php echo $key->create_date ?> </i>
								<p>
								<?php if ($key->image != ""): ?>
									<img src="<?php echo base_url() ?>uploads/komentar/<?php echo $key->image ?>" width="150px"><br>
								<?php endif ?>
								<?php echo $key->isi_komentar ?>
								</p>
							</div>
						</div>
					</div>
					<?php endforeach ?>
					
			    </div>
			    <div role="tabpanel" class="tab-pane" id="bantuan">
					<?php $query_komentar = $model_komentar->getHelpList($model_post->id_post) ?>
					<?php foreach ($query_komentar->result() as $key): ?>
						<div class="comment-item" style="background-color: #FFFAC2">
							<div class="container-fluid">
								<?php if ($key->avatar == "default"): ?>
									<div class="comment-image"><img src="<?php echo base_url() ?>uploads/avatar/default-avatar.png" width="50px"></div>
								<?php else: ?>
									<div class="comment-image"><img src="<?php echo base_url() ?>uploads/avatar/<?php echo $key->avatar ?>" width="50px"></div>
								<?php endif ?>
								<div class="comment-text">
									<b> <?php echo $key->username ?></b> 
									<i> <?php echo $key->create_date ?> </i>
									<p>
									<?php if ($key->image != ""): ?>
										<img src="<?php echo base_url() ?>uploads/komentar/<?php echo $key->image ?>" width="150px"><br>
									<?php endif ?>
									<?php echo $key->isi_komentar ?></p>
								</div>
							</div>
						</div>
					<?php endforeach ?>
			    </div>
			  </div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-5">
		<?php $this->load->view('web/pages/partial/_sidebar-latest-problem') ?>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="<?php echo base_url('app/postHelp/') ?>" method="post" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Poskan Bantuan mu</h4>
      </div>
      <div class="modal-body">
        <!-- <h3>Poskan Bantuan mu</h3> -->
        
        	<label class="control-label"><h4>Message</h4></label>
        	<input type="text" class="form-control col-md-7" name="post_content"></input><br>
        	<label class="control-label"><h4>Photo</h4></label>
        	<input type="file" class="" name="userfile"></input>
        	<input type="hidden" name="id_post" value="<?php echo $model_post->id_post ?>"></input>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="kategori" value="2">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>