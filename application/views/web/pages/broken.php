
<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-8">
		<h2>I'm Sorry Your link is broken.</h2>
		<p class="alert alert-danger">
			Your verification failed or the link is expired
		</p>
	</div>

	<div class="col-xs-12 col-sm- col-md-4">
		<div class="default-wrapper">
			<div class="default-wrapper blue-banner big-padding register-banner">
				<center>
					<img src="<?php echo base_url() ?>assets/img/white-logo.png" class="img-responsive" width="200px">
					<br>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
				</center>
			</div>
			<div class="default-wrapper normal-padding">
				<h3>Already Have an Account ?</h3>
				<form style="max-width:300px;" class="container-fluid" method="post" action="loginme">
					<div class="form-group">
						<input type="text" name="email" placeholder="Email" class="form-control" required>
					</div>
					<div class="form-group">
						<input type="password" name="password" placeholder="Password" class="form-control" required>
					</div>
					<div class="col-md-6">
						<button type="submit" class="btn btn-primary">Login Me !</button> 
					</div>
					<div class="col-md-1" style="line-height: 2.2em">
						Or
					</div>
					<div class="col-md-4 social-register">
						<i class="fa fa-facebook-square"></i>
						<i class="fa fa-twitter-square"></i>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>