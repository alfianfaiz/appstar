<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-8">
		<div class="default-wrapper normal-padding">

		<div>

		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Problem</a></li>
		    <?php if ($this->session->userdata('level')==3): ?>
		    	<li role="presentation"><a href="#user" aria-controls="user" role="tab" data-toggle="tab">User</a></li>
		    <?php endif ?>
		  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content" >
		    <div role="tabpanel" class="tab-pane active" id="problem" style="min-height: 400px;">
				<div class="normal-padding">
					<?php if ($count_problem > 0): ?>
						<h4>Search Result for <?php echo $this->input->post('search') ?></h4>
						<?php foreach ($data_search_problem as $key): ?>
							<div class="media">
								<div class="media-left">
									<div class="fullwidth-image" style="width:200px">
										<center>
											<img src="<?php echo base_url() ?>uploads/post/<?php echo $key->image ?>" height="130px" class="media-object">
										</center>
									</div>
								</div>
								<div class="media-body">
									<h4><a href="<?php echo base_url() ?>app/problem/<?php echo $key->id_post ?>"><?php echo $key->title ?></a></h4>
									<a href="<?php echo base_url() ?>app/profile/<?php echo $key->username ?>"><?php echo $key->username ?></a>
									<div>
										<?php echo $this->Post->countCommentById($key->id_post) ?> Komentar <b><?php echo $this->Post->countHelpById($key->id_post) ?> Bantuan</b>
									</div>
								</div>
								<?php if ($this->session->userdata('level')==2): ?>
									<div class="media-right">
										<center><h2 class="nopadding nomargin"><?php echo $this->Post->countHelpById($key->id_post) ?></h2><b> Bantuan</b></center>
										<?php if ($key->id_petugas == null): ?>
											<a href="<?php echo base_url() ?>app2/tanggapiPost/<?php echo $key->id_post ?>" class="btn btn-danger">Tangani</a>
										<?php else: ?>
											<a href="<?php echo base_url() ?>app2/batalkanTanggapiPost/<?php echo $key->id_post ?>" class="btn btn-info">Batalkan</a>
										<?php endif ?>
									</div>
								<?php endif ?>
							</div>
						<?php endforeach ?>
					<?php else: ?>
						<div class="alert alert-info">tidak ditemukan pencarian dengan keyword <i>"<?php echo $search ?>"</i></div>
					<?php endif ?>
				</div>
		    </div>
		    <?php if ($this->session->userdata('level')==3): ?>
		    	<div role="tabpanel" class="tab-pane" id="user" style="min-height: 400px;">
			    	<!-- <br> -->
					<?php if ($count_user > 0): ?>
						<h4>Search Result for <?php echo $this->input->post('search') ?></h4>
						<?php foreach ($data_search_user as $key): ?>
							<div class="media">
								<div class="media-left">
									<div class="fullwidth-image" style="width:200px">
										<center>
											<img src="<?php echo base_url() ?>uploads/avatar/<?php echo $key->avatar ?>" height="130px" class="media-object">
										</center>
									</div>
								</div>
								<div class="media-body">
									<h4><a href="<?php echo base_url() ?>app/profile/<?php echo $key->username ?>"> <?php echo $key->username ?></a></h4>
									<div>
										232 Post <br><b> 200 Langganan</b><br>
									<?php if ($this->Langganan->checkLangganan($this->session->userdata('id_user'),$key->id_user) > 0): ?>
										<a href="<?php echo base_url() ?>app/berlangganan/<?php echo $key->username ?>" class="btn btn-success btn-sm">
										Berhenti berlangganan</a>
									<?php else: ?>
										<a href="<?php echo base_url() ?>app/berlangganan/<?php echo $key->username ?>" class="btn btn-danger btn-sm">
										Berlangganan</a>
									<?php endif ?>
									</div>
								</div>
							</div>
						<?php endforeach ?>
					<?php else: ?>
						<div class="alert alert-info">tidak ditemukan pencarian dengan keyword <i>"<?php echo $search ?>"</i></div>
					<?php endif ?>
			    </div>
		    <?php endif ?>
		  </div>

		</div>
		</div>
	</div>
</div>