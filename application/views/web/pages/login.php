
<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-8">
		<div class="default-wrapper big-padding">
			<h2>Log In to Appstar</h2>
			<p>Fill details below to Login</p>
			<div class="container-fluid">
				<?php echo validation_errors("<div class=\"col-md-8 alert alert-danger normal-padding\">","</div>"); ?>
				<?php if ($this->session->flashdata('error') != null): ?>
					<div class="col-md-8 alert alert-danger normal-padding"><?php echo $this->session->flashdata('error'); ?></div>
				<?php endif ?>
			<form class="form-horizontal  max400" method="post" action="<?php echo base_url() ?>login/loginme">
				<div class="form-group">
					<div class="col-md-3">
						<label class="control-label">Email</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="email" placeholder="E-mail" class="form-control" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-3">
						<label class="control-label">Password</label>
					</div>
					<div class="col-md-9">
						<input type="password" name="password" placeholder="Password" class="form-control" required>
					</div>
				</div>
				<div class="checkbox">
				    <label>
				      <input type="checkbox" name="remember"> Remember me
				    </label>
				    <button type="submit" class="btn btn-success pull-right">Login Me !</button>
				</div>
			</form>
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm- col-md-4">
		<div class="default-wrapper">
			<div class="default-wrapper blue-banner big-padding register-banner">
				<center>
					<img src="<?php echo base_url() ?>assets/img/white-logo.png" class="img-responsive" width="200px">
					<br>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
				</center>
			</div>
			<div class="default-wrapper normal-padding">
				<h3>Don't have an Account ?</h3>
					<div class="col-md-6">
						<a href="<?php echo base_url() ?>login/register" class="btn btn-primary">Register Me !</a>
					</div>
					<div class="col-md-1" style="line-height: 2.2em">
						Or
					</div>
					<div class="col-md-4 social-register">
						<i class="fa fa-facebook-square"></i>
						<i class="fa fa-twitter-square"></i>
					</div>
			</div>
		</div>
	</div>
</div>