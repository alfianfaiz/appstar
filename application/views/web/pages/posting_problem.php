<style type="text/css">
	.file-upload {
  position: relative;
  overflow: hidden;
  margin: 10px; }

.file-upload input.file-input {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  padding: 0;
  font-size: 20px;
  cursor: pointer;
  opacity: 0;
  filter: alpha(opacity=0); }
	</style>
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script src="<?php echo $base_assets ?>third-js/location-picker/locationpicker.jquery.min.js"></script>
<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7">
	<form method="post" action="<?php echo base_url(); ?>app/post_problem" enctype="multipart/form-data">
		<div class="default-wrapper">
			<div class="fullwidth-image">
				<center>
					<img id="change" src="<?php echo base_url() ?>assets/img/default.png" height="400px">
				</center>
			</div>
			<button  class="file-upload btn btn-change-photo">            
  				<input type="file" name="userfile" id="userfile" class="file-input" onChange="loadfiles()" required> <i class="glyphicon glyphicon-picture"></i> Change Photo
  			</button>

		</div>
		<div class="default-wrapper normal-padding">
			<?php echo validation_errors(); ?>
			<?php echo $this->session->flashdata('error') ?>
			<label class="control-label">Kategori Post</label><br>
			<select name="kategori_post" class="form-control">
				<?php foreach ($data_kategori_post as $key): ?>
					<option value="<?php echo $key->id_kategori_post ?>"><?php echo $key->nama_kategori ?></option>
				<?php endforeach ?>
			</select>
			<br>
			<label class="control-label">Problem Title</label>
			<input class="form-control" type="text" name="title" required></input>
			<br>
			<label class="control-label">Description</label>
			<textarea class="form-control" type="text" name="caption" required></textarea>
			<br>
			<label class="control-label">Location</label>
			<br>
			<button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><span class="glyphicon glyphicon-map-marker"></span></button> Search Location<br><br>
			<input class="form-control" type="text" name="location_description" placeholder="Location Description" required required></input> <br>
			<div class="collapse" id="collapseExample">
			  <div class="well">
			    <div class="form-horizontal" style="width: 550px">
			        <div class="form-group">
			            <label class="col-sm-2 control-label">Location:</label>
			            <div class="col-sm-10">
			                <input type="text" name="location_name" class="form-control" id="us3-address" required />
			            </div>
			        </div>
			        <div id="us3" style="width: 550px; height: 400px;"></div>
			        <div class="clearfix">&nbsp;</div>
			        <div class="m-t-small">
			            <label class="p-r-small col-sm-1 control-label">Lat.:</label>
			            <div class="col-sm-3">
			                <input type="text" name="latitude" class="form-control" style="width: 110px" id="us3-lat" required />
			            </div>
			            <label class="p-r-small col-sm-2 control-label">Long.:</label>
			            <div class="col-sm-3">
			                <input type="text" name="longitude" class="form-control" style="width: 110px" id="us3-lon" required />
			            </div>
			        </div>
			        <div class="clearfix"></div>
			        <script>
			            $('#us3').locationpicker({
			                location: {
			                    latitude: -7.005145300000001,
			                    longitude: 110.43812539999999
			                },
			                radius: 100,
			                inputBinding: {
			                    latitudeInput: $('#us3-lat'),
			                    longitudeInput: $('#us3-lon'),
			                    radiusInput: $('#us3-radius'),
			                    locationNameInput: $('#us3-address')
			                },
			                enableAutocomplete: true,
			                onchanged: function (currentLocation, radius, isMarkerDropped) {
			                    // Uncomment line below to show alert on each Location Changed event
			                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
			                }
			            });
			        </script>
			    </div>
			  </div>
			</div>
			<br>
			<button type="submit" class="btn btn-success">Post Problem</button>
		</div>
	</form>
	</div>
	<div class="col-xs-12 col-sm-5 col-md-5">
		<?php $this->load->view('web/pages/partial/_sidebar-latest-problem') ?>
	</div>
</div>
<script type="text/javascript">
	function loadfiles(){
		var imageFile = document.getElementById('userfile');
		var imagehandler =  document.getElementById('change');
		// if(!imageFile.type.match("image.*"))
		// return
		// imagehandler.src = imageFile.value;
		if(imageFile.files && imageFile.files[0]){
			var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#change').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(imageFile.files[0]);
	        
		}

		
	}
</script>