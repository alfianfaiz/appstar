
<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-8">
		<div class="default-wrapper big-padding">
			<h2>Log In to Appstar</h2>
			<p>Fill details below to Login</p>
			<div class="container-fluid">

			<?php if ($username == "success"): ?>
				<h3>Congratulation, your verification is succeed</h3>
				please login in this <a href="<?php echo base_url() ?>app/login" class="btn btn-success">Link</a>
			<?php else: ?>
				<form class="form-horizontal  max400" method="post" action="<?php echo base_url() ?>login/verifyme">
					<div class="form-group">
						<div class="col-md-4">
							<label class="control-label"><b>Verification Code</b></label>
						</div>
						<div class="col-md-8">
							<input type="hidden" name="username" value="<?php echo $username ?>">
							<input type="text" name="verify_code" placeholder="x x x x x x" class="form-control">
						</div>
					</div>
					<button type="submit" class="btn btn-success pull-right">Verify Me !</button>
				</form>
			<?php endif ?>
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm- col-md-4">
		<div class="default-wrapper">
			<div class="default-wrapper blue-banner big-padding register-banner">
				<center>
					<img src="<?php echo base_url() ?>assets/img/white-logo.png" class="img-responsive" width="200px">
					<br>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
				</center>
			</div>
			<div class="default-wrapper normal-padding">
				<h3>Don't have an Account ?</h3>
					<div class="col-md-6">
						<a href="<?php echo base_url() ?>app/register" class="btn btn-primary">Register Me !</a>
					</div>
					<div class="col-md-1" style="line-height: 2.2em">
						Or
					</div>
					<div class="col-md-4 social-register">
						<i class="fa fa-facebook-square"></i>
						<i class="fa fa-twitter-square"></i>
					</div>
			</div>
		</div>
	</div>
</div>