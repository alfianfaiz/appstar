<div class="row">
	<div class="col-md-9 ">
		<div class="default-wrapper big-padding">
			<div class="wrapper-header">
				<h2 class="">Pengaturan : Profil</h2>
			</div>
			<div class="wrapper-body">
				<div class="thumbnail-me">
					<img src="<?php echo base_url() ?>uploads/avatar/<?php if($model_pengguna->avatar == "default") echo "default-avatar.png"; else echo $model_pengguna->avatar ?>" class="img-responsive img-circle">
				</div>
				<br>
				<button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Change Photo</button> 
				<form action="<?php echo base_url() ?>setting/update" method="post"><br>
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="username" placeholder="username" class="form-control" value="<?php echo $model_pengguna->username ?>">
						<input type="hidden" name="id_user" value="<?php echo $model_pengguna->id_user ?>">
						<input type="hidden" name="old_username" value="<?php echo $model_pengguna->username ?>">
					</div>
					<div class="form-group">
						<label>Full Name</label>
						<input type="text" name="fullname" placeholder="full name" class="form-control" value="<?php echo $model_pengguna->fullname ?>">
					</div>					
					<div class="form-group">
						<label>Address</label>
						<textarea name="address" maxlength="300" class="form-control"><?php echo $model_pengguna->address ?></textarea>
					</div>
					<div class="form-group">
						<label class="control-label">Password</label>
						<input type="password" name="password" placeholder="Kosongkan jika tidak ingin diganti" class="form-control" >
					</div>
					<div class="form-group">
						<label class="control-label">reType Password</label>
						<input type="password" name="password2" placeholder="Kosongkan jika tidak ingin diganti" class="form-control" >
					</div>
					<button type="submit" name="update" class="btn btn-success">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
  <form method="post" action="<?php echo base_url() ?>setting/changephoto" enctype="multipart/form-data">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change your Photo Profile</h4>
      </div>
      <div class="modal-body">
        	<label class="control-label">Choose File !</label>
        	<input type="file" name="userfile"> 
        	<p class="help-block">Just jpg,png,or bmp is accepted</p>
        	<input type="hidden" name="id_user" value="<?php echo $model_pengguna->id_user ?>">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" value="1" name="uploadimage">Save changes</button>
      </div>
    </div>
    </form>
  </div>
</div>