<div class="content-fullwidth">
    <?php if ($keyPage =="all" || $keyPage=='headline'): ?>
        <div class="summary-box-1">
            <h4>Head line</h4>
            <hr>
            <div class="row">
                <div class="container-fluid">
                    <?php foreach ($query_head_line->result() as $key): ?>
                        <div class="col-xs-12 col-sm-3 col-md-3 item-box">
                            <div class="fullwidth-image">
                                <center>
                                    <img src="<?php echo base_url() ?>uploads/post/<?php echo $key->image ?>" height="145px">
                                </center>
                            </div>
                            <h6><a href="<?php echo base_url() ?>app/problem/<?php echo $key->id_post ?>"><?php echo $key->title ?></a></h6>
                            <b><a href="<?php echo base_url() ?>app/profile/<?php echo $key->username ?>" class="box-summary-name"><?php echo $key->username ?></a></b>
                            <p class="box-summary-comment"><?php echo $key->A ?> Komentar</p>
                        </div>    
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    <?php endif ?>
    <?php if ($keyPage =="all" || $keyPage=='latest_problem'): ?>
        <div class="summary-box-1">
            <h4>Latest Problem</h4>
            <hr>
            <div class="row">
                <div class="container-fluid">
                    <?php foreach ($query_last->result() as $key): ?>
                        <div class="col-xs-12 col-sm-3 col-md-3 item-box">
                            <div class="fullwidth-image">
                                <center>
                                    <img src="<?php echo base_url() ?>uploads/post/<?php echo $key->image ?>" height="145px">
                                </center>
                            </div>
                            <h6><a href="<?php echo base_url() ?>app/problem/<?php echo $key->id_post ?>"><?php echo $key->title ?></a></h6>
                            <b><a href="<?php echo base_url() ?>app/profile/<?php echo $key->username ?>" class="box-summary-name"><?php echo $key->username ?></a></b>
                            <p class="box-summary-comment"><?php echo $key->A ?> Komentar</p>
                        </div>    
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>