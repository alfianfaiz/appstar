<?php 
	$sudah_ditangani = $base_model_pengguna->getAllPostDitanganiForPetugas($base_model_pengguna->id_user);
	$belum_ditangani = $base_model_pengguna->getAllPostBelumDitanganiForPetugas($base_model_pengguna->id_user);
	$semua = $base_model_pengguna->getAllPostForPetugas($base_model_pengguna->id_user);
	$active = "active";
	if ($id_kategori_post !=null) {
		$pencarian = $base_model_pengguna->getAllPostPencarianForPetugas($base_model_pengguna->id_user,$id_kategori_post);
		$pencarian_active = "active";
		$active = "";
	}
?>
<div class="row">
	<div class="col-xs-12 col-sm-8 col-md-8">
		<div class="default-wrapper normal-padding">

			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs" role="tablist">
			    <?php if ($id_kategori_post != null): ?>
				<li role="presentation" class="active"><a href="#pencarian" aria-controls="pencarian" role="tab" data-toggle="tab" >Pencarian "<?php echo $kategori_post ?>"</a></li>
				<?php endif ?>

			    <li role="presentation" class="<?php echo $active ?>" ><a href="#belum" aria-controls="belum" role="tab" data-toggle="tab">Belum Ditangani</a></li>
			    <li role="presentation"><a href="#ditangani" aria-controls="ditangani" role="tab" data-toggle="tab">Sudah Ditangani</a></li>
			    <li role="presentation"><a href="#semua" aria-controls="semua" role="tab" data-toggle="tab">Semua</a></li>
			  </ul>

			  <!-- Tab panes -->
			  <div class="tab-content">
			    <div role="tabpanel" class="tab-pane <?php echo $active ?>" id="belum">
			    	<div class="normal-padding">
			    		<?php foreach ($belum_ditangani as $key): ?>
				    		<div class="media">
								<div class="media-left">
									<div class="fullwidth-image" style="width:200px">
										<center>
											<img src="<?php echo base_url() ?>uploads/post/<?php echo $key->image ?>" height="130px" class="media-object">
										</center>
									</div>
								</div>
								<div class="media-body">
									<h4><a href="<?php echo base_url() ?>app/problem/<?php echo $key->id_post ?>"><?php echo  $key->title ?></a></h4>
									<description><?php echo  $key->caption ?></description>
									<div>
										<i class="fa fa-map-marker" style="color:red"></i> <?php echo  $key->location_name ?>
									</div>
								</div>
								<div class="media-right">
									<center><h2 class="nopadding nomargin"><?php echo $this->Post->countHelpById($key->id_post) ?></h2><b> Bantuan</b></center>
									<a href="<?php echo base_url() ?>app2/tanggapiPost/<?php echo $key->id_post ?>" class="btn btn-danger">Tangani</a>
								</div>
							</div><!--  -->
						<?php endforeach ?>
						<?php if (count($belum_ditangani) == 0): ?>
							Tidak Ada Post
						<?php endif ?>
			    	</div>
			    </div>
			    <div role="tabpanel" class="tab-pane" id="ditangani">
					<div class="normal-padding">
						<?php foreach ($sudah_ditangani as $key): ?>
				    		<div class="media">
								<div class="media-left">
									<div class="fullwidth-image" style="width:200px">
										<center>
											<img src="<?php echo base_url() ?>uploads/post/<?php echo $key->image ?>" height="130px" class="media-object">
										</center>
									</div>
								</div>
								<div class="media-body">
									<h4><a href="<?php echo base_url() ?>app/problem/<?php echo $key->id_post ?>"><?php echo  $key->title ?></a></h4>
									<description><?php echo  $key->caption ?></description>
									<div>
										<i class="fa fa-map-marker" style="color:red"></i> <?php echo  $key->location_name ?>
									</div>
								</div>
								<div class="media-right">
									<center><h2 class="nopadding nomargin"><?php echo $this->Post->countHelpById($key->id_post) ?></h2><b> Bantuan</b></center>
									<a href="<?php echo base_url() ?>app2/batalkanTanggapiPost/<?php echo $key->id_post ?>" class="btn btn-info">Batalkan</a>
								</div>
							</div><!--  -->
						<?php endforeach ?>
						<?php if (count($sudah_ditangani) == 0): ?>
							Tidak Ada Post
						<?php endif ?>
					</div>
			    </div>
			    <div role="tabpanel" class="tab-pane" id="semua">
			    	<div class="normal-padding">
			    		<?php foreach ($semua as $key): ?>
				    		<div class="media">
								<div class="media-left">
									<div class="fullwidth-image" style="width:200px">
										<center>
											<img src="<?php echo base_url() ?>uploads/post/<?php echo $key->image ?>" height="130px" class="media-object">
										</center>
									</div>
								</div>
								<div class="media-body">
									<h4><a href="<?php echo base_url() ?>app/problem/<?php echo $key->id_post ?>"><?php echo  $key->title ?></a></h4>
									<description><?php echo  $key->caption ?></description>
									<div>
										<i class="fa fa-map-marker" style="color:red"></i> <?php echo  $key->location_name ?>
									</div>
								</div>
								<div class="media-right">
									<center><h2 class="nopadding nomargin"><?php echo $this->Post->countHelpById($key->id_post) ?></h2><b> Bantuan</b></center>
									<?php if ($key->id_petugas == null): ?>
										<a href="<?php echo base_url() ?>app2/tanggapiPost/<?php echo $key->id_post ?>" class="btn btn-danger">Tangani</a>
									<?php else: ?>
										<a href="<?php echo base_url() ?>app2/batalkanTanggapiPost/<?php echo $key->id_post ?>" class="btn btn-info">Batalkan</a>
									<?php endif ?>
								</div>
							</div><!--  -->
						<?php endforeach ?>
						<?php if (count($semua) == 0): ?>
							Tidak Ada Post
						<?php endif ?>
			    	</div>
			    </div>
			    <?php if ($id_kategori_post != null): ?>
				<div role="tabpanel" class="tab-pane active" id="pencarian">
					<div class="normal-padding">
			    		<?php foreach ($pencarian as $key): ?>
				    		<div class="media">
								<div class="media-left">
									<div class="fullwidth-image" style="width:200px">
										<center>
											<img src="<?php echo base_url() ?>uploads/post/<?php echo $key->image ?>" height="130px" class="media-object">
										</center>
									</div>
								</div>
								<div class="media-body">
									<h4><a href="<?php echo base_url() ?>app/problem/<?php echo $key->id_post ?>"><?php echo  $key->title ?></a></h4>
									<description><?php echo  $key->caption ?></description>
									<div>
										<i class="fa fa-map-marker" style="color:red"></i> <?php echo  $key->location_name ?>
									</div>
								</div>
								<div class="media-right">
									<center><h2 class="nopadding nomargin"><?php echo $this->Post->countHelpById($key->id_post) ?></h2><b> Bantuan</b></center>
									<?php if ($key->id_petugas == null): ?>
										<a href="<?php echo base_url() ?>app2/tanggapiPost/<?php echo $key->id_post ?>" class="btn btn-danger">Tangani</a>
									<?php else: ?>
										<a href="<?php echo base_url() ?>app2/batalkanTanggapiPost/<?php echo $key->id_post ?>" class="btn btn-info">Batalkan</a>
									<?php endif ?>
								</div>
							</div><!--  -->
						<?php endforeach ?>
						<?php if (count($pencarian) == 0): ?>
							Tidak Ada Post
						<?php endif ?>
			    	</div>
			    </div>		
				<?php endif ?>
			    
			  </div>

		</div>
	</div>
</div>