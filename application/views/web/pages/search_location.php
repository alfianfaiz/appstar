<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script src="<?php echo $base_assets ?>third-js/location-picker/locationpicker.jquery.min.js"></script>

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7">
		<div class="default-wrapper normal-padding">
			<!-- <div class="form-horizontal" style="width: 550px"> -->

		            
			        
			        <div id="us3" style="width: 550px; height: 400px;"></div>
			        
			        <br>
			        <label class="control-label"><b>Search</b> </label>
		            <input type="text" name="location_name" class="form-control" id="us3-address" style="max-width: 550px" required />
		            <br>
		            <div class="col-md-3">
		            	<label class="control-label">Latitude :</label><br>
		            	<input type="text" name="latitude" class="form-control" style="width: 110px" id="us3-lat" required />
		            </div>
		            <div class="col-md-3">
		            	<label class="control-label">Longitude :</label><br>
			            <input type="text" name="longitude" class="form-control" style="width: 110px" id="us3-lon" required />

		            </div>
			        <div class="clearfix"></div>
			        <script>
			            $('#us3').locationpicker({
			                location: {
			                    latitude: -7.005145300000001,
			                    longitude: 110.43812539999999
			                },
			                radius: 100,
			                inputBinding: {
			                    latitudeInput: $('#us3-lat'),
			                    longitudeInput: $('#us3-lon'),
			                    radiusInput: $('#us3-radius'),
			                    locationNameInput: $('#us3-address')
			                },
			                enableAutocomplete: true,
			                onchanged: function (currentLocation, radius, isMarkerDropped) {
			                    // Uncomment line below to show alert on each Location Changed event
			                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
			                }
			            });
			        </script>
			
			<!-- <div class="form-inline normal-padding">
				<div class="form-group">
					<input type="text" name="mapsearch" placeholder="Search here" class="form-control">
				</div>
				<button class="btn btn-primary"><i class="fa fa-map-marker"></i>Cari</button>
			</div>
			<div class="location-search-result">
				
			</div> -->
		</div>
	</div>

	<div class="col-xs-12 col-sm-5 col-md-5">
		<?php $this->load->view('web/pages/partial/_sidebar-latest-problem') ?>
	</div>
</div>