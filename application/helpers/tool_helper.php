<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Common Tool
*/
class Tool
{
	public function generateExcerpt($text,$numb) {
		$text = strip_tags($text);
		if (strlen($text) > $numb) { 
		  $text = substr($text, 0, $numb); 
		  $text = substr($text,0,strrpos($text," ")); 
		  $etc = " ...";  
		  $text = $text.$etc; 
		  }
		echo $text; 
	}
}
 ?>