<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifikasi extends CI_Model {

	public $id_notifikasi;
	public $post_id_post;
	public $langganan_id_langganan;
	public $kategori_notif_id_kategori_notif;
	public $create_date;
	public $pengguna_id_user;
	
	public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }

	public function creteNotif(){
		$this->db->insert('notifikasi',$this);
	}

	public function createNotif($data){
		$this->db->insert('notifikasi',$data);	
	}

	public function getUserNotif($id_user,$limit){
		$query = "SELECT notifikasi.create_date,
		notifikasi.id_notifikasi, 
		post.caption,
		pengguna.username,
		kategori_notif.pesan,
		post.id_post
		FROM kategori_notif,notifikasi,post,pengguna where notifikasi.post_id_post = post.id_post AND post.user_id_user = pengguna.id_user AND kategori_notif.id_kategori_notif = notifikasi.kategori_notif_id_kategori_notif AND pengguna.id_user = 1
			UNION 
			SELECT 
			notifikasi.create_date,
			notifikasi.id_notifikasi,
			'null',
			pengguna.username,
			kategori_notif.pesan,
			langganan.user_id_user
			 from kategori_notif,notifikasi,pengguna,langganan WHERE notifikasi.langganan_id_langganan = langganan.id_langganan AND langganan.user_id_user = pengguna.id_user  AND kategori_notif.id_kategori_notif = notifikasi.kategori_notif_id_kategori_notif  AND pengguna.id_user = 1
			ORDER BY `create_date` DESC";
		return $this->db->query($query);
	}

}

/* End of file Notifikasi.php */
/* Location: ./application/models/Notifikasi.php */