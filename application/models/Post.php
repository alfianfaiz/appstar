<?php 
class Post extends CI_Model{
	public $id_post;
	public $user_id_user;
	public $image;
	public $title;
	public $caption;
	public $location_name;
	public $location_description;
	public $longitude;
	public $latitude;
	public $create_date;

	public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();

    }

    public function insert_entry(){
    	// echo $this->create_date = date('Y/m/d',mktime(0,0,0,10,8,1995));  	
    	$this->db->insert('post',$this);
    }

    public function getData($id){
    	$this->db->where('id_post',$id);
    	$query = $this->db->get('post');
    	foreach ($query->result() as $key ) {
    		$this->id_post = $id;
			$this->user_id_user = $key->user_id_user;
			$this->image = $key->image;
			// $this->title = $key->title;
			$this->caption = $key->caption;
			$this->location_name = $key->location_name;
			$this->location_description = $key->location_description;
			$this->longitude = $key->longitude;
			$this->latitude = $key->latitude;
			$this->create_date = $key->create_date;
    	}
    }

    public function getDataByUserId($id_user){
    	$this->db->where('user_id_user',$id_user);
    	$query = $this->db->get('post');
    	return $query;
    }

    public function countCommentById($id_post){
    	$this->db->where('post_id_post',$id_post);
    	$query = $this->db->get('komentar');
    	$result = count($query->result());
    	return $result;
    }

    public function countHelpById($id_post){
        $this->db->where('post_id_post',$id_post);
        $this->db->where('kategori',2);
        $query = $this->db->get('komentar');
        $result = count($query->result());
        return $result;
    }

    public function getLatestProblem($limit){
        $sql = "SELECT post.*,post.create_date as Time,count(komentar.id_komentar) as A,pengguna.username as username 
                FROM `komentar`, `post`,`pengguna` 
                WHERE komentar.post_id_post = post.id_post 
                AND post.user_id_user = pengguna.id_user 
                UNION
                SELECT post.*,post.create_date as Time, count(null) as A,pengguna.username as username 
                FROM `post`,pengguna
                where post.user_id_user = pengguna.id_user
                GROUP BY post.id_post 
                ORDER BY Time  DESC LIMIT $limit";
        return $this->db->query($sql);
    }



    public function getHeadline($limit){
        $sql = "SELECT post.*,post.create_date as Time,count(komentar.id_komentar) as A,pengguna.username as username 
                FROM `komentar`, `post`,`pengguna` 
                WHERE komentar.post_id_post = post.id_post 
                AND post.user_id_user = pengguna.id_user 
                UNION
                SELECT post.*,post.create_date as Time, count(null) as A,pengguna.username as username 
                FROM `post`,pengguna
                where post.user_id_user = pengguna.id_user
                GROUP BY post.id_post 
                ORDER BY A DESC,Time  DESC LIMIT $limit";
        return $this->db->query($sql);
    }

    public function getProblemPage($id_post){
        $sql = "SELECT post.image as image, 
                post.title as title,
                post.caption as caption, 
                post.create_date as create_date,
                post.id_post as id_post,
                pengguna.username as username,
                pengguna.id_user as id_pengguna,
                pengguna.avatar as avatar,
                post.location_name as location_name
                FROM pengguna,post
                WHERE 
                pengguna.id_user = post.user_id_user
                AND post.id_post = $id_post
                ";
        $query = $this->db->query($sql);
        return $query->result()[0];
    }

    public function countBantuan(){
        $this->db->where('post_id_post',$this->id_post);
        $this->db->where('kategori',2);
        $query = $this->db->get('komentar');
        return count($query->result());
    }
    
}

 ?>