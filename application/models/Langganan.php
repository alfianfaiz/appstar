<?php 
class Langganan extends CI_Model{
	public $id_langganan;
	public $user_id_user;
	public $user_id_user_2;
	public $create_date;

	public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();

    }

    public function berlangganan($user_berlangganan,$user_target){
    	$data = array(
            'user_id_user' => $user_berlangganan,
            'user_id_user_2' => $user_target,
            );
    	$this->db->insert('langganan',$data);
    }

     public function hapusBerlangganan($user_berlangganan,$user_target){
        $this->db->where('user_id_user',$user_berlangganan);
        $this->db->where('user_id_user_2',$user_target);
        $this->db->delete('langganan');
    }

    public function checkLangganan($user_berlangganan,$user_target){
        $this->db->where('user_id_user',$user_berlangganan);
        $this->db->where('user_id_user_2',$user_target);
        $query = $this->db->get('langganan');
        return count($query->result());
    }

    public function getPelanggan($user_target){
        $this->db->where('user_id_user_2',$user_target);
        $query = $this->db->get('langganan');
        return $query;
    }   

    public function getBerlangganan($user_berlangganan){
        $this->db->where('user_id_user',$user_berlangganan);
        $this->db->join('pengguna','langganan.user_id_user_2 = pengguna.id_user');
        $query = $this->db->get('langganan');
        return $query;
    } 
}

 ?>