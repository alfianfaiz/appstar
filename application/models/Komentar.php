<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  Komentar extends CI_Model {

	public $id_komentar;
	public $post_id_post;
	public $user_id_user;
	public $isi_komentar;
	public $kategori;
	public $image;
	public $create_date;

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function getCommentAndHelpList($id_post=null)
	{
		$this->db->where('post_id_post', $id_post);
		$this->db->order_by('komentar.create_date','DESC');
		$this->db->join('pengguna','pengguna.id_user = komentar.user_id_user');
		$query = $this->db->get('komentar');
		return $query;
	}

	public function getHelpList($id_post=null)
	{
		$this->db->where('post_id_post', $id_post);
		$this->db->join('pengguna','pengguna.id_user = komentar.user_id_user');
		$this->db->where('kategori',2);
		$query = $this->db->get('komentar');
		return $query;
	}

	public function insertData(){
		if($this->db->insert('komentar',$this))
			return TRUE;
		else return FALSE;
	}

}

/* End of file Komentar.php */
/* Location: ./application/models/Komentar.php */