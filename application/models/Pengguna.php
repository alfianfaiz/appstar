<?php 
class Pengguna extends CI_Model{
	public $id_user;
	public $username;
	public $fullname;
	public $email;
	public $city;
	public $address;
	public $password;
	public $notif_counter;
	public $create_date;
	public $avatar;
	public $verified;
	public $verify_code;
    public $level;
    public $kategori_petugas_id;

	public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();

    }

    public function insert_entry(){
    	// echo $this->create_date = date('Y/m/d',mktime(0,0,0,10,8,1995));
    	$this->notif_counter = 0;
    	$this->avatar = "default-avatar.png";
    	$this->verified = 0;    	
    	$this->db->insert('pengguna',$this);
    }

    public function getData($id){
    	$this->db->where('id_user',$id);
    	$query = $this->db->get('pengguna');
    	foreach ($query->result() as $key ) {
    		$this->id_user = $id;
			$this->username = $key->username;
			$this->fullname = $key->fullname;
			$this->email = $key->email;
			$this->city = $key->city;
			$this->address = $key->address;
			$this->password = $key->password;
			$this->notif_counter = $key->notif_counter;
			$this->create_date = $key->create_date;
			$this->avatar = $key->avatar;
			$this->verified = $key->verified;
			$this->verify_code = $key->verify_code;
            $this->kategori_petugas_id = $key->kategori_petugas_id;
    	}
    }

    public function getDataByUsername($username){
    	$this->db->where('username',$username);
    	$query = $this->db->get('pengguna');
    	$cek = 0;
    	foreach ($query->result() as $key ) {
    		$cek ++;
    		$this->id_user = $key->id_user;
			$this->username = $key->username;
			$this->fullname = $key->fullname;
			$this->email = $key->email;
			$this->city = $key->city;
			$this->address = $key->address;
			$this->password = $key->password;
			$this->notif_counter = $key->notif_counter;
			$this->create_date = $key->create_date;
			$this->avatar = $key->avatar;
			$this->verified = $key->verified;
			$this->verify_code = $key->verify_code;
    	}
    	if($cek == 0) return FALSE;
    		return TRUE;
    }

    public function getProblemPosted(){
    	$counted = count($this->getUserPost()->result());
    	return $counted;
    }

    public function getUserBerlangganan(){
    	$this->db->where('user_id_user_2',$this->id_user);
    	$query = $this->db->get('langganan');
    	$result = count($query->result());
    	return $result;
    }

    public function getUserPelanggan(){
    	$this->db->where('user_id_user',$this->id_user);
    	$query = $this->db->get('langganan');
    	$result = count($query->result());
    	return $result;
    }

    public function getUserPost(){
    	$this->load->model('Post');
    	$queryResult = $this->Post->getDataByUserId($this->id_user);
    	return $queryResult;
    }

    public function getPetugas($key = null){
        $this->db->where('level',2);
        if ($key != null) {
            $this->db->like('username', $key);
            // $this->db->or_like('email', $key);
        }
        $this->db->join('kategori_petugas', 'pengguna.kategori_petugas_id = kategori_petugas.id_kategori_petugas');
        $queryResult = $this->db->get('pengguna');
        return $queryResult;
    }

    public function getPetugasByKategori($id_kategori){
        $this->db->where('kategori_petugas_id',$id_kategori);
        $queryResult = $this->db->get('pengguna');
        return $queryResult;   
    }

    public function getRandomPelanggan(){
    	// $this->db->where('id_user',"NOT ".$this->session->userdata('id_user'));
    	$this->db->order_by('id_user', 'RANDOM');
    	$this->db->limit(5);
    	$query = $this->db->get('pengguna');
    	return $query;
    }

    public function getKategoriDitangani($id_kategori_petugas){
        $this->db->join('petugas_post', 'petugas_post.kategori_post_id_kategori_post = kategori_post.id_kategori_post');
        $this->db->where('petugas_post.kategori_petugas_id_kategori_petugas', $id_kategori_petugas);
        return $query_kategori = $this->db->get('kategori_post')->result();
    }

    public function getNotifikasi($id_user,$limit){
        $this->load->model('Notifikasi');
        return $this->Notifikasi->getUserNotif($id_user,1);
    }

    public function deletePetugas($id){
        $this->db->where('id_user', $id);
        $this->db->delete('pengguna');
    }

    public function getAllPostForPetugas($id_petugas){
        $sql = "SELECT * FROM `post` WHERE post.kategori_post_id_kategori_post IN (
            SELECT kategori_post.id_kategori_post FROM `kategori_post`
            LEFT JOIN (petugas_post)
            ON (petugas_post.kategori_post_id_kategori_post = kategori_post.id_kategori_post) 
            WHERE petugas_post.kategori_petugas_id_kategori_petugas = $id_petugas
        )";
        return $query_post = $this->db->query($sql)->result();
    }

    public function getAllPostDitanganiForPetugas($id_petugas){
        $sql = "SELECT * FROM `post` WHERE post.kategori_post_id_kategori_post IN (
            SELECT kategori_post.id_kategori_post FROM `kategori_post`
            LEFT JOIN (petugas_post)
            ON (petugas_post.kategori_post_id_kategori_post = kategori_post.id_kategori_post) 
            WHERE petugas_post.kategori_petugas_id_kategori_petugas = $id_petugas
            ) AND post.id_petugas IS NOT NULL
        ";
        return $query_post = $this->db->query($sql)->result();
    }

    public function getAllPostBelumDitanganiForPetugas($id_petugas){
        $sql = "SELECT * FROM `post` WHERE post.kategori_post_id_kategori_post IN (
            SELECT kategori_post.id_kategori_post FROM `kategori_post`
            LEFT JOIN (petugas_post)
            ON (petugas_post.kategori_post_id_kategori_post = kategori_post.id_kategori_post) 
            WHERE petugas_post.kategori_petugas_id_kategori_petugas = $id_petugas
            ) AND post.id_petugas IS NULL
        ";
        return $query_post = $this->db->query($sql)->result();
    }

    public function getAllPostPencarianForPetugas($id_petugas,$id_kategori_post){
        $sql = "SELECT * FROM `post` WHERE post.kategori_post_id_kategori_post IN (
            SELECT kategori_post.id_kategori_post FROM `kategori_post`
            LEFT JOIN (petugas_post)
            ON (petugas_post.kategori_post_id_kategori_post = kategori_post.id_kategori_post) 
            WHERE petugas_post.kategori_petugas_id_kategori_petugas = $id_petugas
            ) AND post.kategori_post_id_kategori_post = '$id_kategori_post'
        ";
        return $query_post = $this->db->query($sql)->result();
    }


}

 ?>