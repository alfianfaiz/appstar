<?php 
/**
* Login Class
*/
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Pengguna');
		
	}

	public function index(){
		$checkRemember = $this->checkCookies();
		if($checkRemember != false){
			$this->rememberedLogin($checkRemember,get_cookie('apstr'));
			return 0;
		}
		$data = array(
			'page' => 'login',
			'title' => 'Login to Appstar'
			);
		$this->load->view('web/layout/applayout-without-sidebar.php',$data);
		if($this->session->userdata('IS_LOGGED_IN')){
			if ($this->session->userdata('level')==2) {
				redirect('app2/');
			} else redirect('app/');
		}

	}

	public function admin(){
		$data = array(
			'page' => 'login',
			'title' => 'Login to Appstar'
			);
		$this->load->view('admin/adminlogin',$data);
		if($this->session->userdata('IS_LOGGED_IN'))
			redirect('app/');
	}

	private function rememberedLogin($id_user,$code){
		$this->db->where('id_user',$id_user);
		$data_user = $this->db->get('pengguna', 1, 0)->result();
		foreach ($data_user as $q) {
			$dataDB=array(
				'IS_LOGGED_IN' =>TRUE , 
				'id_user'=> $q->id_user, 
				'fullname'=> $q->fullname, 
				'username'=> $q->username, 
				'email'=> $q->email,
				'avatar' => $q->avatar,
				'level' => $q->level,
				'kategori_petugas_id' => $q->kategori_petugas_id,
			);
			$this->session->set_userdata($dataDB);
			$this->remember($id_user,$code);
		}
		if ($this->session->userdata('level') == 1) {
			redirect('admin','refresh');
			echo "1";
		}
		redirect('app','refresh');
		echo "2";
	}

	public function adminlogin(){
		if($_POST){
						
			$this->form_validation->set_rules('email', 'Email', 'required');
        	$this->form_validation->set_rules('password', 'Password', 'required');

        	if ($this->form_validation->run() == TRUE) {
        		$ciphertext = md5($this->input->post('password'));
        		$this->db->where('email',$this->input->post('email'));
        		$this->db->where('password',$ciphertext);
        		$this->db->where('level', 1);
        		$this->db->where('verified',1);
        		$query = $this->db->get('pengguna');
        		foreach ($query->result() as $q) {
        			$dataDB=array(
						'IS_LOGGED_IN' =>TRUE , 
						'id_user'=> $q->id_user, 
						'fullname'=> $q->fullname, 
						'username'=> $q->username, 
						'email'=> $q->email,
						'avatar' => $q->avatar,
						'level' => $q->level,
						'kategori_petugas_id' => $q->kategori_petugas_id,
					);
					$this->session->set_userdata($dataDB);
					if ($this->input->post('remember') == "on") {
						$this->remember($q->id_user);
					}
        		}
        		if ($this->session->userdata('level') == 1) {
        			redirect('admin','refresh');
        			echo "1";
        		}
				redirect('login/admin','refresh');
				echo "2";

        	} else {
        		echo validation_errors();
        		redirect('login/admin','refresh');
        	}
		} 
		else
			redirect('login/admin');
			echo "3";
	}

	public function loginme(){

		if($_POST){			
			$this->form_validation->set_rules('email', 'Email', 'required');
        	$this->form_validation->set_rules('password', 'Password', 'required');

        	if ($this->form_validation->run() == TRUE) {
        		$ciphertext = md5($this->input->post('password'));
        		$this->db->where('email',$this->input->post('email'));
        		$this->db->where('password',$ciphertext);
        		$this->db->where_in('level', array(2,3));
        		$this->db->where('verified',1);
        		$query = $this->db->get('pengguna');
        		foreach ($query->result() as $q) {
        			$dataDB=array(
						'IS_LOGGED_IN' =>TRUE , 
						'id_user'=> $q->id_user, 
						'fullname'=> $q->fullname, 
						'username'=> $q->username, 
						'email'=> $q->email,
						'avatar' => $q->avatar,
						'level' => $q->level,
						'kategori_petugas_id' => $q->kategori_petugas_id,
					);
					$this->session->set_userdata($dataDB);
					if ($this->input->post('remember') == "on") {
						$this->remember($q->id_user);
					}
        		}
        		if ($this->session->userdata('level') == 2) {
        			redirect('app2','refresh');
        		}
        		$this->session->set_flashdata('error', 'email atau password yang anda masukkan salah');
				$this->index();

        	} else {
        		$this->session->set_flashdata('error', 'email atau password yang anda masukkan salah');
        		$this->index();
        	}
		} 
		else
			redirect('login');
	}

	public function apilogin(){

		$ciphertext = md5($this->input->get('password'));
		$this->db->where('email',$this->input->get('email'));
		$this->db->where('password',$ciphertext);
		
		$this->db->where('verified',1);
		$query = $this->db->get('pengguna');
		foreach ($query->result() as $q) {
			$dataDB=array(array(
				'validasi' => 1 , 
				'id_user'=> $q->id_user, 
				'fullname'=> $q->fullname, 
				'username'=> $q->username, 
				'email'=> $q->email,
				'avatar' => $q->avatar,
				'address' => $q->address,
				'password' => $q->password,
			));
			// print_r($dataDB);
			echo json_encode($dataDB);
			return TRUE;
			// $this->session->set_userdata($dataDB);
		}
		$response["validasi"] = 0;
		echo json_encode(array($response));
		return FALSE;

	}

	public function logout(){
		$this->load->helper('cookie');
		$this->session->sess_destroy();
		$this->db->where('token',get_cookie('apstr'));
		$this->db->delete('active_session');
		delete_cookie('apstr');
        redirect(base_url());

	}

	public function register(){
		$data = array(
			'page' => 'register',
			'title' => 'Register to Appstar'
			);
		// echo "yes";
		$this->load->view('web/layout/applayout-without-sidebar.php',$data);
	}

	public function registerme(){
		$this->load->library('Swiftmailer');
		$senderMail = new Swiftmailer();

		if($this->input->post('register')){

			$this->form_validation->set_rules('username', 'username', 'trim|required|alpha_numeric|min_length[5]|max_length[32]|is_unique[pengguna.username]');
			$this->form_validation->set_rules('fullname', 'fullname', 'trim|required');
			$this->form_validation->set_rules('password2', 'password2', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]|max_length[12]|matches[password2]');
			$this->form_validation->set_rules('address', 'address', 'trim|required|min_length[5]|max_length[160]');
			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[pengguna.email]');

			if($this->form_validation->run() == FALSE) {
				$this->register();
			} else {
				# code...
				$this->load->model('pengguna');
				$model_pengguna = new Pengguna();
			

				$model_pengguna->username = strtolower($this->input->post('username'));
				$model_pengguna->fullname = $this->input->post('fullname');
				$model_pengguna->city = "Semarang";
				$model_pengguna->email = $this->input->post('email');
				$model_pengguna->password = md5($this->input->post('password'));
				$model_pengguna->address = $this->input->post('address');
				$model_pengguna->level = 3;
				$model_pengguna->verify_code = $this->generateCode();
				$model_pengguna->insert_entry();

				$verify_code = $model_pengguna->verify_code;
				$link = base_url()."login/verifyme/".$model_pengguna->username;
				
				$email = $model_pengguna->email;
				$sendmail = $senderMail->sendMail("<h2>Thanks for Register</h2>$verify_code,$link","Please Confirm",$email);
				redirect("login/thanksforregister");
			}
		} else redirect('register');
	}

	public function thanksforregister(){
		$data = array(
			'page' => 'thanksforregister',
			'title' => 'Thanks For Register'
			);
		$this->load->view('web/layout/applayout-without-sidebar.php',$data);
	}

	public function verifyme($username = "none"){
		if($_POST){
			$this->db->where('username',$this->input->post('username'));
			$this->db->where('verify_code',$this->input->post('verify_code'));
			$this->db->where('verified',0);
			$query = $this->db->get('pengguna');
			$cek = count($query->result());
			
			if($cek == 1 ){
				$this->db->where('username',$this->input->post('username'));
				$this->db->where('verify_code',$this->input->post('verify_code'));
				$this->db->set('verified',1);
				$this->db->set('verify_code',0);
				$this->db->update('pengguna');
				redirect('login/verifyme/success');
			} else redirect('login/brokenlink');
		}
		$data = array(
			'page' => 'verifyme',
			'title' => 'Thanks For Register',
			'username' => $username,
			);
		$this->load->view('web/layout/applayout-without-sidebar.php',$data);
	}

	public function brokenlink(){
		$data = array(
			'page' => 'broken',
			'title' => 'Broken Link',
			);
		$this->load->view('web/layout/applayout-without-sidebar.php',$data);
	}

	public function generateCode(){
		// $this->load->library('database');
		$this->load->library('RandomCode');
		$randomCode = new RandomCode;
		$RC = $randomCode->RandomPass(3,3);
		$this->db->where('verify_code',$RC);
		if($this->db->count_all_results('pengguna') == 0)
			return $RC;
		else $this->generateCode();
	}

	private function remember($id_user,$code = null){
		$this->load->helper('cookie');
		$this->load->library('user_agent');
		if ($this->agent->is_browser())
		{
		        $agentbrowser = $this->agent->browser().$this->agent->version();
		}
		if ($this->agent->is_mobile())
		{
		        $agentbrowser = $this->agent->mobile().$this->agent->version();
		}
		$os = $this->agent->platform();
		if ($code == null) {
			$code =  $this->generateCodeRemember();
			$data = array(
				'user_id_user' => $id_user,
				'browser' => $agentbrowser,
				'os' => $os,
				'token' => $code,
				);
			$this->db->insert('active_session',$data);
		}
		$cookie = array(
			    'name'   => 'apstr',
			    'value'  => $code,
			    'expire' => '1209600',  // Two weeks
			);
		set_cookie($cookie);
		return true;
	}


	function checkCookies(){
		$this->load->helper('cookie');
		$this->load->library('user_agent');
		$cookie = get_cookie('apstr');
		$this->db->where('token',$cookie);
		$this->db->select('user_id_user,browser');
		$querysession = $this->db->get('active_session')->result();
		foreach ($querysession as $key) {
			$check = $key;
			if ($this->agent->is_browser())
			{
			        $agentbrowser = $this->agent->browser().$this->agent->version();
			}
			if ($this->agent->is_mobile())
			{
			        $agentbrowser = $this->agent->mobile().$this->agent->version();
			}
			if($check->browser == $agentbrowser) return $check->user_id_user;
			else return false;
		}
		return false;
	}

	function generateCodeRemember(){
		$this->load->library('RandomCode');
		$randomCode = new RandomCode;
		$RC = $randomCode->RandomPass(5,5);
		$this->db->where('token',$RC);
		if($this->db->count_all_results('active_session') == 0)
			return $RC;
		else $this->generateCodeRemember();
	}
}

 