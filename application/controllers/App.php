<?php 
/**
* NameFile : App.php
* CLass App 
*/
class App extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('IS_LOGGED_IN'))
			redirect('login');
		else $this->load->model('Pengguna');
		$this->checkPetugas();
		$this->load->helper('Tool');
	}

	function checkPetugas(){
		if($this->session->userdata('level')==2){
			redirect('app2/','refresh');
		}
	}

	public function index($keyPage = "all"){
		$this->checkPetugas();
		$this->load->model('Post');
		$query_head_line = "";
		$query_latest ="";
		switch ($keyPage) {
			case 'headline':
				$query_head_line = $this->Post->getHeadline(20);
				break;
			case 'latest':
				$query_latest = $this->Post->getLatestProblem(20);
				break;
			default:
				$query_head_line = $this->Post->getHeadline(4);
				$query_latest = $this->Post->getLatestProblem(4);
				break;
		}
		$data = array(
			'page' => 'home',
			'title' => 'home',
			'keyPage' => $keyPage,
			'query_head_line' => $query_head_line,
			'query_last' => $query_latest,
			);
		$this->load->view('web/layout/applayout.php',$data);
	}
	public function search_location(){
		$data = array(
			'page' => 'search_location',
			'title' => 'Search Location'
			);
		$this->load->view('web/layout/applayout.php',$data);
	}
	public function search_problem($search = null){
		$this->load->model('post');
		$this->load->model('langganan');
		$query_search = "";
		if(isset($_POST['search'])){
			redirect('app/search_problem/'.$this->input->post('search'),'refresh');
		}

		if ($search != null) {
			$this->db->or_like('title',$search);
			$this->db->or_like('caption',$search);
			$this->db->join('pengguna', 'user_id_user = pengguna.id_user');
			$data_search_problem = $this->db->get('Post')->result();
			$count_problem  = count($data_search_problem);

			$this->db->or_like('username',$search);
			$data_search_user = $this->db->get('Pengguna')->result();
			$count_user  = count($data_search_user);
		}

		$data = array(
			'page' => 'search_problem',
			'title' => 'Search Problem',
			'data_search_problem' => $data_search_problem,
			'data_search_user' => $data_search_user,
			'count_problem' => $count_problem,
			'count_user' => $count_user,
			'search' => $search,
			);
		$this->load->view('web/layout/applayout.php',$data);
	}
	public function problem($id_post = null){
		if ($id_post == null) {
			redirect('app/index','refresh');
		}

		$this->load->model('Post');
		$this->load->model('Komentar');
		$this->load->model('langganan');
		$model_post = new Post();
		$model_post->getData($id_post);
		$data_post = $model_post->getProblemPage($id_post);
		$model_komentar = new Komentar();
		$data = array(
			'page' => 'problem',
			'title' => 'Problem',
			'data_post' => $data_post,
			'model_post'=>$model_post,
			'model_komentar' => $model_komentar
			);
		// print_r($data_post);
		$this->load->view('web/layout/applayout.php',$data);
	}
	public function posting_problem(){
		$kategori_post = $this->db->get('kategori_post')->result();
		$data = array(
			'page' => 'posting_problem',
			'title' => 'Post New Problem',
			'data_kategori_post' => $kategori_post,
			);
		$this->load->view('web/layout/applayout.php',$data);
	}
	public function post_problem(){
		if($_POST){
			print_r($this->input->post('userfile'));
			$title = $this->input->post('title');
			$caption = $this->input->post('caption');
			$id_user =  $this->session->userdata('id_user');
			$location_name = $this->input->post('location_name');
			$latitude = $this->input->post('latitude');
			$longitude = $this->input->post('longitude');
			$location_description = $this->input->post('location_description');

			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[256]');
			$this->form_validation->set_rules('caption', 'Caption', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('kategori_post', 'Caption', 'trim|required|numeric');
			// $this->form_validation->set_rules('userfile', 'userfile', 'trim|required');

			if ($this->form_validation->run() == TRUE) {
				echo "Post it";

				$fileName = $id_user.date('Ymdhis').'.jpg';
				$config['upload_path'] 			= './uploads/post/';
				$config['allowed_types']        = 'png|jpg|jpeg|bmp';
		        $config['max_size']             = 2000;
		        $config['file_name']            = $fileName;
		        $config['overwrite']			= TRUE;

		        // initializing library upload after config
		        $this->load->library('upload', $config);

		        // uploading file
		        if ( ! $this->upload->do_upload('userfile'))
		        {
		            $error = array('error' => $this->upload->display_errors());
		            $message = "";
		            foreach ($error as $key) {
		            	echo $message .= "<li>".$key."</li>";
		            }
		            $this->session->set_flashdata('error',$message);
		            // redirect('app/posting_problem');
		        } else{
		        	$data = array(
		        		'user_id_user' => $id_user,
		        		'image' => $fileName,
		        		'caption' => $caption,
		        		'location_name' => $location_name,
		        		'longitude' => $longitude,
		        		'latitude' => $latitude,
		        		'location_description' => $location_description,
		        		'kategori_post_id_kategori_post' => $this->input->post('kategori_post'),
		        		);
		        	$this->db->insert('post',$data);
		        	redirect('app/problem/'.$this->db->insert_id());
		        }

			} else {
				echo "fail";
				echo validation_errors();
			}
		}
		// redirect('app/posting_problem');
	}
	public function postHelp()
	{
		if($_POST){
			$this->load->model('Komentar');
			$this->load->model('Notifikasi');
			$this->form_validation->set_rules('post_content', 'Isi Komentar', 'trim|required|min_length[1]|max_length[600]');
			$this->form_validation->set_rules('id_post', 'Post', 'required');

			if ($this->form_validation->run() == TRUE) {
				$model_komentar = new Komentar();
				
				if ($this->input->post('kategori') == 1) {
					$model_komentar->post_id_post = $this->input->post('id_post');
					$model_komentar->user_id_user = $this->session->userdata('id_user');
					$model_komentar->isi_komentar = $this->input->post('post_content');
					$model_komentar->kategori = 1;
					$model_komentar->image = "";
					$model_komentar->insertData();
					$data_notif = array(
						'post_id_post' => $this->input->post('id_post'),
						'kategori_notif_id_kategori_notif' => 2,
						'pengguna_id_user' => $this->session->userdata('id_user'));
					$this->Notifikasi->createNotif($data_notif);
					redirect('app/problem/'.$this->input->post('id_post'));
				} elseif($this->input->post('kategori') == 2) {
					$id_user = $this->session->userdata('username');
					$fileName = $id_user.date('Ymdhis').'.jpg';
					$config['upload_path'] 			= './uploads/komentar/';
					$config['allowed_types']        = 'png|jpg|jpeg|bmp';
			        $config['max_size']             = 2000;
			        $config['file_name']            = $fileName;
			        $config['overwrite']			= TRUE;

			        // initializing library upload after config
			        $this->load->library('upload', $config);

			        // uploading file
			        if ( ! $this->upload->do_upload('userfile'))
			        {
			            $error = array('error' => $this->upload->display_errors());
			            $message = "";
			            foreach ($error as $key) {
			            	echo $message .= "<li>".$key."</li>";
			            }
			            $this->session->set_flashdata('error',$message);
			            redirect('app/problem/'.$this->input->post('id_post'));
			        } else{
			        	$model_komentar->post_id_post = $this->input->post('id_post');
						$model_komentar->user_id_user = $this->session->userdata('id_user');
						$model_komentar->isi_komentar = $this->input->post('post_content');
						$model_komentar->image = $fileName;
						$model_komentar->kategori = 2;
						$model_komentar->insertData();
						$data_notif = array(
							'post_id_post' => $this->input->post('id_post'),
							'kategori_notif_id_kategori_notif' => 4,
							'pengguna_id_user' => $this->session->userdata('id_user'));
						$this->Notifikasi->createNotif($data_notif);
						redirect('app/problem/'.$this->input->post('id_post'));
			        }				
				}
				
			} else {
				redirect('app/problem/'.$this->input->post('id_post'));
			}



		}
	}

	public function about(){
		$data_appearance = $this->db->get('appearance')->result();
		$data = array(
			'page' => 'about',
			'title' => 'About',
			'data_appearance' => $data_appearance[0],
			);
		$this->load->view('web/layout/applayout.php',$data);
	}

	public function login(){
		if($this->session->userdata('IS_LOGGED_IN'))
			redirect('app/');
		$data = array(
			'page' => 'login',
			'title' => 'Login to Appstar'
			);
		$this->load->view('web/layout/applayout-without-sidebar.php',$data);
	}
	public function profile($username = null){
		if($username == null){
			redirect('app/index','refresh');
		}
		// $this->load->model('Pengguna');
		$this->load->model('Post');
		// $this->load->model('Langganan');
		$model_pengguna = new Pengguna();
		$model_post = new Post();
		$model_langganan = new Langganan();
		if($model_pengguna->getDataByUsername($username) == FALSE)
			redirect('app/index','refresh');

		$data = array(
			'page' => 'profile',
			'title' => 'Profile',
			'model_pengguna' => $model_pengguna,
			'model_post' => $model_post,
			'model_langganan' => $model_langganan,
			);
		$this->load->view('web/layout/applayout.php',$data);
	}

	public function berlangganan($username=null){
		if($username == null)
		{
			redirect('app/','refresh');
		}else {
			// $this->load->model('Pengguna');
			$model_pengguna = new Pengguna();
			if(!$model_pengguna->getDataByUsername($username)){
				redirect('app/','refresh');
			}

			// $this->load->model('Langganan');
			$model_langganan = new Langganan();
			$cek = $model_langganan->checkLangganan($this->session->userdata('id_user'),$model_pengguna->id_user);
			if($cek > 0) 
			{
				$model_langganan->hapusBerlangganan($this->session->userdata('id_user'),$model_pengguna->id_user);
				redirect('app/profile/'.$username,'refresh');
			}
			else 
			{
				$model_langganan->berlangganan($this->session->userdata('id_user'),$model_pengguna->id_user);
				redirect('app/profile/'.$username,'refresh');
			}
		}
	}

	public function cobaGMAP(){
		$url = "http://maps.google.com/maps/api/geocode/json?address=pekalongan&sensor=false&";
		$response = file_get_contents($url);
		$response = json_decode($response, true);
		 
		//print_r($response);
		 
		$lat = $response['results'][0]['geometry']['location']['lat'];
		$long = $response['results'][0]['geometry']['location']['lng'];
		 
		echo "latitude: " . $lat . " longitude: " . $long;
	}


}
 ?>