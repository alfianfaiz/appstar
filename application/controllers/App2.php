<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App2 extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('IS_LOGGED_IN'))
			redirect('login');
		if($this->session->userdata('level')==3){
			redirect('app2/','refresh');
		}
		 $this->load->model('Pengguna');
		 $this->load->library('user_agent');
	}

	public function index($kategori_post = null)
	{
		$this->load->model('post');
		$this->load->helper('inflector');
		$this->load->model('post');
		$id_kategori_post = null;
		if ($kategori_post != null) {
			$kategori_post = humanize($kategori_post,'_');
			$id_kategori_post = $this->getIdKategoriPost($kategori_post);
		}
		$this->load->model('Post');

		$data = array(
			'page' => 'petugas/home',
			'title' => 'Home - Petugas',
			'id_kategori_post' => $id_kategori_post,
			'kategori_post' => $kategori_post,
			);
		$this->load->view('web/layout/applayout.php',$data);
	}

	function getIdKategoriPost($nama_kategori){
		$this->db->where('nama_kategori', $nama_kategori);
		$this->db->select('id_kategori_post');
		$queryKategoriPost = $this->db->get('kategori_post')->result();
		$id_kategori_post = 0;
		foreach ($queryKategoriPost as $key) {
			$id_kategori_post = $key->id_kategori_post;
		}
		return $id_kategori_post;
	}

	public function tanggapiPost($id_post){
		$this->db->where('id_post', $id_post);
		$this->db->set('id_petugas', $this->session->userdata('id_user'));
		$this->db->update('post');
		redirect($this->agent->referrer());
	}

	public function batalkanTanggapiPost($id_post){
		$this->db->where('id_post', $id_post);
		$this->db->set('id_petugas', NULL);
		$this->db->update('post');
		redirect($this->agent->referrer());
	}

	public function suskesMenanggapi(){
		echo "Anda Sukses Menanggapi";
	}

	public function arrayidkategoripost(){
		$kategori = $this->Pengguna->getKategoriDitangani($this->session->userdata('id_user'));
		$datakategori = array();
		foreach ($kategori as $key) {
			array_push($datakategori,$key->id_kategori_post);
		}
		return $datakategori;
	}

	public function search_problem($search = null){
		$query_search = "";
		$this->load->model('Post');
		if(isset($_POST['search'])){
			redirect('app2/search_problem/'.$this->input->post('search'),'refresh');
		}
		if($search != null){
			$arrayidkategoripost = $this->arrayidkategoripost();
			$like = array('title'=>$this->input->post('search'),'caption'=>$this->input->post('search'));
			$this->db->group_start();
			$this->db->or_like($like);
			$this->db->group_end();
			$this->db->join('pengguna', 'user_id_user = pengguna.id_user');
			$this->db->where_in('kategori_post_id_kategori_post',$arrayidkategoripost);
			$data_search_problem = $this->db->get('post')->result();
			$count_problem  = count($data_search_problem);

			$this->db->or_like('username',$this->input->post('search'));
			$data_search_user = $this->db->get('Pengguna')->result();
			$count_user  = count($data_search_user);
		}
		$data = array(
			'page' => 'search_problem',
			'title' => 'Search Problem',
			'data_search_problem' => $data_search_problem,
			'data_search_user' => $data_search_user,
			'count_problem' => $count_problem,
			'count_user' => $count_user,
			);
		$this->load->view('web/layout/applayout.php',$data);
	}
}

/* End of file App2.php */
/* Location: ./application/controllers/App2.php */