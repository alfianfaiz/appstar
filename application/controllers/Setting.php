<?php 
	/**
	* Setting Class
	*/
	class Setting extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->cekLogin();
		}

		function cekLogin(){
			if(!$this->session->userdata('IS_LOGGED_IN'))
			redirect('app/login');
		}

		public function index(){
			$this->load->model('Pengguna');
			$model_pengguna = new Pengguna();
			$model_pengguna->getData($this->session->userdata('id_user'));
			$data = array(
				'page' => 'setting',
				'title' => 'Setting Profile',
				'model_pengguna' => $model_pengguna,
				);
			$this->load->view('web/layout/applayout.php',$data);
		}

		public function update(){
			if($_POST){
				

				$this->form_validation->set_rules('username', 'username', 
					array(
						'required',
						'alpha_numeric',
						'min_length[5]',
						'max_length[32]',
						array('username_callable',function($str)
				        {
				                if ($str == $this->input->post('old_username'))
				                {
				                    return TRUE;
				                }
				                else
				                {		
				                	$this->db->where('username',strtolower($str));
				                	$query = $this->db->get('pengguna');
				                	$count = count($query->result());
				                	if($count >0) {
				                		$this->form_validation->set_message('username_callable', 'The  is already taken');
				                		return FALSE;
				                	}
				                        return TRUE;
				                }
				        }
						))
				);

				$this->form_validation->set_rules('fullname', 'fullname', 'trim|required');
				$this->form_validation->set_rules('address', 'address', 'trim|required|min_length[5]|max_length[160]');

				if ($this->form_validation->run() == TRUE) {
					if ($this->input->post('password')) {
						$this->form_validation->set_rules('password2', 'password2', 'trim|required');
						$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]|max_length[12]|matches[password2]');
						echo "yes update with password";
						if ($this->form_validation->run() == TRUE) {
							$this->db->set('password',md5($this->input->post('password')));
						} else {
							echo validation_errors();
							// $this->index();
						}
					}

					$this->db->where('id_user',$this->input->post('id_user'));
					$this->db->set('username',$this->input->post('username'));
					$this->db->set('fullname',$this->input->post('fullname'));
					$this->db->set('address',$this->input->post('address'));
					$this->db->update('pengguna');
					redirect('setting/');
				} else {
					echo "fail";
					echo validation_errors();
				}
			}
		}
		public function username_check($str,$str2)
        {

                if ($str != $str2)
                {
                	// $this->db->where('username',$str);
                	// $query = $this->db->get('pengguna');
                	// echo $count = count($query->result());
                        $this->form_validation->set_message('username_check', 'The {field} is already taken');
                 //        echo $str;
                	echo $str.'and'.$str2;
                        return FALSE;

                }
                else
                {
                        return TRUE;
                }
        }

        public function changePhoto(){
        	if($this->input->post('uploadimage')){
        		$id_user = $this->input->post('id_user');
        		$this->load->model('pengguna');
        		$model_pengguna = new Pengguna();
        		$model_pengguna->getData($id_user);
        		if($model_pengguna->avatar == "default-avatar.png")
        		{$fileName = $id_user.date('Ymdhis').'.jpg';}
				else $fileName = $model_pengguna->avatar;
				$config['upload_path'] 			= './uploads/avatar/';
				$config['allowed_types']        = 'png|jpg|jpeg|bmp';
		        $config['max_size']             = 2000;
		        $config['file_name']            = $fileName;
		        $config['overwrite']			= TRUE;

		        // initializing library upload after config
		        $this->load->library('upload', $config);

		        // uploading file
		        if ( ! $this->upload->do_upload('userfile'))
		        {
		            $error = array('error' => $this->upload->display_errors());
		            $message = "";
		            foreach ($error as $key) {
		            	echo $message .= "<li>".$key."</li>";
		            }
		            $this->session->set_flashdata('error',$message);
		            redirect('setting/','refresh');
		        } else{
		        	$this->db->where('id_user',$id_user);
		        	$this->db->set('avatar',$fileName);
		        	$this->db->update('pengguna');
		        	redirect('setting/','refresh');
		        }
		        
			}
        }
	}
 ?>