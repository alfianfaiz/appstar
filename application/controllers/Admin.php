<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if(!($this->session->userdata('IS_LOGGED_IN') && $this->session->userdata('level') == 1))
			redirect('login/admin','refresh');
		
	}
	public function index()
	{
		$querycheck = $this->db->get('appearance')->result();
		if(count($querycheck) == 0){
			$this->initappearance();
		}
		foreach ($this->db->get('appearance')->result() as $key) {
			$data_appearance = $key;
		}
		// print_r($data_appearance);
		$data = array(
			'page' => 'appearance',
			'page_title' => 'Appearance',
			'data_appearance' => $data_appearance
			);
		$this->load->view('admin/layoutadmin.php', $data);
	}
	public function fitur(){
		$data = array(
			'page' => 'fitur',
			'page_title' => 'Fitur',
			);
		$this->load->view('admin/layoutadmin.php', $data);
	}
	public function user($search = null){
		$this->load->helper('inflector');
		$data_search = null;
		if($search != null){
			$search = humanize($search);
			$this->db->like('username', $search);
			$data_search = $this->db->get('pengguna')->result();
		}
		
		if ($this->input->post('search') != null) {
			redirect('admin/user/'.underscore($this->input->post('search')),'refresh');
		}
		$data = array(
			'page' => 'user',
			'page_title' => 'User',
			'data_search' => $data_search,
			);

		$this->load->view('admin/layoutadmin.php', $data);
	}
	public function sitemap(){
		$data = array(
			'page' => 'Sitemap',
			'page_title' => 'Sitemap',
			);
		$this->load->view('admin/layoutadmin.php', $data);
	}
	public function tentang()
	{
		$querycheck = $this->db->get('appearance')->result();
		if(count($querycheck) == 0){
			$this->initappearance();
		}
		foreach ($this->db->get('appearance')->result() as $key) {
			$data_appearance = $key;
		}

		$data = array(
			'page' => 'tentang',
			'page_title' => 'Tentang',
			'data_appearance' => $data_appearance,
			);
		$this->load->view('admin/layoutadmin.php', $data);
	}
	public function simpantentang(){
		if (isset($_POST['tentang_content'])) {
			
			$this->db->set('tentang',$this->input->post('tentang_content'));
			$this->db->update('appearance');
			redirect('admin/tentang','refresh');
		}
	}
	public function developer(){
		$data = array(
			'page' => 'developer',
			'page_title' => 'Developer',
			);
		$this->load->view('admin/layoutadmin.php', $data);
	}
	public function petugas(){
		$this->load->model('Pengguna');
		$queryPetugas = $this->Pengguna->getPetugas();
		if ($this->input->post('caripetugas') != NULL) {
			$queryPetugas = $this->Pengguna->getPetugas($this->input->post('key_cari_petugas'));
		}
		$queryKategoriPetugas = $this->db->get('kategori_petugas');
		$data = array(
			'page' => 'petugas',
			'page_title' => 'Kelola Petugas',
			'data_petugas' => $queryPetugas,
			'data_kategori_petugas' => $queryKategoriPetugas,
			);
		$this->load->view('admin/layoutadmin.php', $data);
	}
	public function tambah_petugas(){
		$queryKategoriPetugas = $this->db->get('kategori_petugas');
		$data = array(
			'page' => 'tambah_petugas',
			'page_title' => 'Tambah Petugas',
			'data_kategori_petugas' => $queryKategoriPetugas,
			);
		$this->load->view('admin/layoutadmin.php', $data);
	}
	public function delete_petugas($id = null){
		// Re Login
		$this->load->model('Pengguna');
		if ($id != null) {
			$this->Pengguna->deletePetugas($id);
		}
		redirect('admin/petugas','refresh');
	}
	public function edit_petugas($id_user = null){
		$queryKategoriPetugas = $this->db->get('kategori_petugas');
		$this->load->model('pengguna');
		$model_pengguna = new Pengguna();
		if($id_user != null){
			$model_pengguna->getData($id_user);
		}
		$data = array(
			'page' => 'edit_petugas',
			'page_title' => 'Edit Data Petugas',
			'data_kategori_petugas' => $queryKategoriPetugas,
			'model_pengguna' => $model_pengguna,
			);
		$this->load->view('admin/layoutadmin.php', $data);
	}

	public function update_petugas(){
		if($this->input->post('register')){

			$this->form_validation->set_rules('username', 'username', 
					array(
						'required',
						'alpha_numeric',
						'min_length[5]',
						'max_length[32]',
						array('username_callable',function($str)
				        {
				                if ($str == $this->input->post('old_username'))
				                {
				                    return TRUE;
				                }
				                else
				                {		
				                	$this->db->where('username',strtolower($str));
				                	$query = $this->db->get('pengguna');
				                	$count = count($query->result());
				                	if($count >0) {
				                		$this->form_validation->set_message('username_callable', 'The  is already taken');
				                		return FALSE;
				                	}
				                        return TRUE;
				                }
				        }
						))
				);
			$this->form_validation->set_rules('kategori_petugas', 'fullname', 'trim|required');
			$this->form_validation->set_rules('fullname', 'fullname', 'trim|required|min_length[5]|max_length[40]');
			$this->form_validation->set_rules('address', 'address', 'trim|required|min_length[5]|max_length[160]');
			$this->form_validation->set_rules('email', 'email', 
					array(
						'required',
						'min_length[5]',
						'max_length[40]',
						'valid_email',
						array('email_callable',function($str)
				        {
				                if ($str == $this->input->post('old_email'))
				                {
				                    return TRUE;
				                }
				                else
				                {		
				                	$this->db->where('email',strtolower($str));
				                	$query = $this->db->get('pengguna');
				                	$count = count($query->result());
				                	if($count >0) {
				                		$this->form_validation->set_message('email_callable', 'The  is already taken');
				                		return FALSE;
				                	}
				                        return TRUE;
				                }
				        }
						))
				);

			if($this->form_validation->run() == FALSE) {
				// echo validation_errors();
				// echo "validation fail";
				$this->edit_petugas($this->input->post('id_user'));
			} else {
				# code...

				if ($this->input->post('password')) {
					$this->form_validation->set_rules('password2', 'password2', 'trim|required');
					$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]|max_length[12]|matches[password2]');
					echo "yes update with password";
					if ($this->form_validation->run() == TRUE) {
						$this->db->set('password',md5($this->input->post('password')));
					} else {
						$this->edit_petugas($this->input->post('id_user'));
					}
				}
				$this->db->where('id_user',$this->input->post('id_user'));
				$this->db->set('username',$this->input->post('username'));
				$this->db->set('fullname',$this->input->post('fullname'));
				$this->db->set('address',$this->input->post('address'));
				$this->db->set('kategori_petugas_id',$this->input->post('kategori_petugas'));
				$this->db->update('pengguna');
				redirect("admin/edit_petugas/".$this->input->post('id_user'));
				// $this->edit_petugas($this->input->post('id_user'));
			}
		} else 
		$this->edit_petugas($this->input->post('id_user'));
		// redirect("admin/edit_petugas/".$this->input->post('id_user'));
	}
	public function edit_pengguna($id_user = null){
		$queryKategoriPetugas = $this->db->get('kategori_petugas');
		$this->load->model('pengguna');
		$model_pengguna = new Pengguna();
		if($id_user != null){
			$model_pengguna->getData($id_user);
		}
		$data = array(
			'page' => 'edit_pengguna',
			'page_title' => 'Edit Data Pengguna',
			'model_pengguna' => $model_pengguna,
			);
		$this->load->view('admin/layoutadmin.php', $data);
	}
	public function update_pengguna(){
		if($this->input->post('register')){

			$this->form_validation->set_rules('username', 'username', 
					array(
						'required',
						'alpha_numeric',
						'min_length[5]',
						'max_length[32]',
						array('username_callable',function($str)
				        {
				                if ($str == $this->input->post('old_username'))
				                {
				                    return TRUE;
				                }
				                else
				                {		
				                	$this->db->where('username',strtolower($str));
				                	$query = $this->db->get('pengguna');
				                	$count = count($query->result());
				                	if($count >0) {
				                		$this->form_validation->set_message('username_callable', 'The  is already taken');
				                		return FALSE;
				                	}
				                        return TRUE;
				                }
				        }
						))
				);
			$this->form_validation->set_rules('fullname', 'fullname', 'trim|required|min_length[5]|max_length[40]');
			$this->form_validation->set_rules('address', 'address', 'trim|required|min_length[5]|max_length[160]');
			$this->form_validation->set_rules('email', 'email', 
					array(
						'required',
						'min_length[5]',
						'max_length[40]',
						'valid_email',
						array('email_callable',function($str)
				        {
				                if ($str == $this->input->post('old_email'))
				                {
				                    return TRUE;
				                }
				                else
				                {		
				                	$this->db->where('email',strtolower($str));
				                	$query = $this->db->get('pengguna');
				                	$count = count($query->result());
				                	if($count >0) {
				                		$this->form_validation->set_message('email_callable', 'The  is already taken');
				                		return FALSE;
				                	}
				                        return TRUE;
				                }
				        }
						))
				);

			if($this->form_validation->run() == FALSE) {
				// echo validation_errors();
				// echo "validation fail";
				$this->edit_pengguna($this->input->post('id_user'));
			} else {
				# code...

				if ($this->input->post('password')) {
					$this->form_validation->set_rules('password2', 'password2', 'trim|required');
					$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]|max_length[12]|matches[password2]');
					echo "yes update with password";
					if ($this->form_validation->run() == TRUE) {
						$this->db->set('password',md5($this->input->post('password')));
					} else {
						$this->edit_pengguna($this->input->post('id_user'));
					}
				}
				$this->db->where('id_user',$this->input->post('id_user'));
				$this->db->set('username',$this->input->post('username'));
				$this->db->set('fullname',$this->input->post('fullname'));
				$this->db->set('address',$this->input->post('address'));
				$this->db->set('kategori_petugas_id',$this->input->post('kategori_petugas'));
				$this->db->update('pengguna');
				redirect("admin/edit_pengguna/".$this->input->post('id_user'));
				// $this->edit_petugas($this->input->post('id_user'));
			}
		} else 
		$this->edit_pengguna($this->input->post('id_user'));
		// redirect("admin/edit_petugas/".$this->input->post('id_user'));
	}
	public function register_petugas(){
		$this->load->library('Swiftmailer');
		$senderMail = new Swiftmailer();

		if($this->input->post('register')){

			$this->form_validation->set_rules('username', 'username', 'trim|required|min_length[5]|max_length[32]|is_unique[pengguna.username]');
			$this->form_validation->set_rules('kategori_petugas', 'fullname', 'trim|required');
			$this->form_validation->set_rules('fullname', 'fullname', 'trim|required');
			$this->form_validation->set_rules('password2', 'password2', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[5]|max_length[12]|matches[password2]');
			$this->form_validation->set_rules('address', 'address', 'trim|required|min_length[5]|max_length[160]');
			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[pengguna.email]');

			if($this->form_validation->run() == FALSE) {
				// $this->register();
				echo validation_errors();
				echo "validation fail";
				redirect("admin/tambah_petugas");
			} else {
				# code...
				$this->load->model('pengguna');
				$model_pengguna = new Pengguna();
			

				$model_pengguna->username = $this->input->post('username');
				$model_pengguna->fullname = $this->input->post('fullname');
				$model_pengguna->city = "Semarang";
				$model_pengguna->email = $this->input->post('email');
				$model_pengguna->password = md5($this->input->post('password'));
				$model_pengguna->address = $this->input->post('address');
				$model_pengguna->level = 2;
				$model_pengguna->kategori_petugas_id = $this->input->post('kategori_petugas');
				$model_pengguna->verify_code = $this->generateCode();
				$model_pengguna->insert_entry();

				$verify_code = $model_pengguna->verify_code;
				$link = base_url()."login/verifyme/".$model_pengguna->username;
				
				$email = $model_pengguna->email;
				$sendmail = $senderMail->sendMail("<h2>Thanks for Register</h2>$verify_code,$link","Please Confirm",$email);
				echo " you did it";
				redirect("admin/petugas");
			}
		} else //echo "nothing sent";//
		redirect('admin/tambah_petugas');
	}

	public function kelolapenanganan($id = null){
		$pencarian = $this->getNamaKategoriPost($id);

		if(isset($_POST['kategori_post'])){
			// $id_kategori_post = $this->getNamaKategoriPost($_POST['kategori_post']);
			redirect('admin/kelolapenanganan/'.$this->input->post('kategori_post'),'refresh');
		}

		$queryKategoriPetugas = $this->db->get('kategori_petugas');
		$queryKategoriPost = $this->db->get('kategori_post');
		
		if($pencarian != "Tidak Ada"){
			// $pencarian = $this->input->post('kategori_post');
			$sql = "SELECT 
					*
					FROM kategori_petugas 
					WHERE 
					kategori_petugas.nama_kategori NOT IN (
					    SELECT kategori_petugas.nama_kategori FROM kategori_petugas,kategori_post,`petugas_post`
					    WHERE 
					    kategori_petugas.id_kategori_petugas = petugas_post.kategori_petugas_id_kategori_petugas AND
					    kategori_post.id_kategori_post = petugas_post.kategori_post_id_kategori_post AND
					    kategori_post.id_kategori_post= '".$id."'
					    GROUP BY kategori_petugas.id_kategori_petugas
					)";
			$queryKategoriPetugas = $this->db->query($sql);

			$this->db->join('kategori_petugas', 'petugas_post.kategori_petugas_id_kategori_petugas = kategori_petugas.id_kategori_petugas');
			$this->db->select('kategori_petugas.nama_kategori as nama_kategori,petugas_post.id_petugas_post');
			$this->db->join('kategori_post', 'petugas_post.kategori_post_id_kategori_post = kategori_post.id_kategori_post');
			$this->db->where('kategori_post.id_kategori_post', $id);
		}
		$this->db->group_by('kategori_petugas_id_kategori_petugas');
		$query_petugas_post = $this->db->get('petugas_post');
		$data = array(
			'page' => 'penanganan',
			'page_title' => 'Penanganan',
			'data_kategori_petugas' => $queryKategoriPetugas,
			'data_kategori_post' => $queryKategoriPost,
			'data_petugas_post' => $query_petugas_post,
			'pencarian' => $pencarian,
			'id_kategori_post' => $id,
			);
		$this->load->view('admin/layoutadmin.php', $data);
	}

	public function searchkategoripetugas(){
		if ($this->input->is_ajax_request()) {
			$this->db->select('kategori_petugas.nama_kategori as nama_kategori,petugas_post.id_petugas_post');
			$this->db->join('kategori_petugas', 'petugas_post.kategori_petugas_id_kategori_petugas = kategori_petugas.id_kategori_petugas');
			$this->db->join('kategori_post', 'petugas_post.kategori_post_id_kategori_post = kategori_post.id_kategori_post');
			$this->db->where('kategori_post.nama_kategori', $this->input->post('kategori_post'));
			$this->db->group_by('kategori_petugas_id_kategori_petugas');
			$query_petugas_post = $this->db->get('petugas_post');
			$data_petugas_post = $query_petugas_post->result();
			$no = 0;
			if(count($data_petugas_post) > 0)
			{
				foreach ($data_petugas_post as $key) {
					$no++;
					echo "	<tr>
								<td>$no</td>
								<td>$key->nama_kategori</td>
								<td><button class=\"btn btn-default btn-sm\"><a href=\"\">Hapus</a></button></td>
							</tr>";
				}
			}
			else echo "Kosong";
		} else echo "Not Found";
	}

	public function generateCode(){
		// $this->load->library('database');
		$this->load->library('RandomCode');
		$randomCode = new RandomCode;
		$RC = $randomCode->RandomPass();
		$this->db->where('verify_code',$RC);
		if($this->db->count_all_results('pengguna') == 0)
			return $RC;
		else $this->generateCode();
	}

	public function setwebtitle()
	{
		if (isset($_POST['web_title'])) {
			
			$this->db->set('title',$this->input->post('web_title'));
			$this->db->update('appearance');
			redirect('admin/index','refresh');
		}
	}

	public function bismillah(){
		$data = array(
			'page' => 'dicacak',
			'page_title' => 'Tambah Petugas',
			
			);
		$this->load->view('admin/dicacak.php', $data);
	}

	public function addpetugaspenagan(){
		echo $this->input->post('nama_kategori_post');
		echo $this->input->post('kategori_petugas');
		$id_kategori_post = $this->getIdKategoriPost($this->input->post('nama_kategori_post'));
		$data = array(
			'kategori_post_id_kategori_post' => $id_kategori_post,
			'kategori_petugas_id_kategori_petugas' => $this->input->post('kategori_petugas'));
		$this->db->insert('petugas_post', $data);
		redirect('admin/kelolapenanganan/'.$id_kategori_post,'refresh');
	}

	function getIdKategoriPost($nama_kategori){
			$this->db->where('nama_kategori', $nama_kategori);
			$this->db->select('id_kategori_post');
			$queryKategoriPost = $this->db->get('kategori_post')->result();
			$id_kategori_post = 0;
			foreach ($queryKategoriPost as $key) {
				$id_kategori_post = $key->id_kategori_post;
			}
			return $id_kategori_post;
	}

	function getNamaKategoriPost($id_kategori_post ){
			$nama_kategori = "Tidak Ada";
			$this->db->where('id_kategori_post', $id_kategori_post);
			$this->db->select('nama_kategori');
			$queryKategoriPost = $this->db->get('kategori_post')->result();
			$id_kategori_post = 0;
			foreach ($queryKategoriPost as $key) {
				$nama_kategori = $key->nama_kategori;
			}
			return $nama_kategori;
	}

	public function savefooter()
	{
		if (isset($_POST['footer_content'])) {
			
			$this->db->set('footer_text',$this->input->post('footer_content'));
			$this->db->update('appearance');
			redirect('admin/index','refresh');
		}
	}
	public function savedeskripsi()
	{
		if (isset($_POST['deskripsi'])) {
			
			$this->db->set('deskripsi',$this->input->post('deskripsi'));
			$this->db->update('appearance');
			redirect('admin/index','refresh');
		}
	}

	public function changelogo()
	{
		if (isset($_POST['savelogo'])) {
			
			$fileName = 'appstarlogo.png';
				$config['upload_path'] 			= './assets/img/';
				$config['allowed_types']        = 'png|jpg|jpeg|bmp';
		        $config['max_size']             = 2000;
		        $config['file_name']            = $fileName;
		        $config['overwrite']			= TRUE;

		        // initializing library upload after config
		        $this->load->library('upload', $config);

		        // uploading file
		        if ( ! $this->upload->do_upload('userfile'))
		        {
		            $error = array('error' => $this->upload->display_errors());
		            $message = "";
		            foreach ($error as $key) {
		            	echo $message .= "<li>".$key."</li>";
		            }
		            $this->session->set_flashdata('error',$message);
		            // redirect('app/posting_problem');
		        } else{
		        	$this->db->set('logo',$fileName);
					$this->db->update('appearance');
					redirect('admin/index','refresh');
		        }
			
		}
	}

	public function changecolor(){
		if(isset($_POST['sticky_nav_color'])){
			$this->db->set('stick_color',$this->input->post('sticky_nav_color'));
			$this->db->set('sidebar_color',$this->input->post('sidebar_color'));
			$this->db->set('background',$this->input->post('background_color'));
			$this->db->update('appearance');
			redirect('admin/index','refresh');
		}
	}

	function initappearance(){
		$data = array(
			'title' => "APPSTAR",
			'logo' => "appstarlogo.png",
			'background' => "#000",
			'sidebar_color'  => "#000",
			'stick_color' => "#000",
			'footer_text' => "Dummy Text",
			'maintenance' => 0,
			'api_mobile' => 1,
			);
		$this->db->insert('appearance', $data);
	}

	public function savesitemap(){
		if ($this->input->post('sitemap_content') != NULL) {
			if($this->checkPage('sitemap') == 0){
				$dataSitemap = array(
					'user_id_user' => $this->session->userdata('id_user'),
					'title_page' => "Sitemap",
					'description_page' => "Map for Appstar Site",
					'content_page' => "Dummy Text",
					'kategori' => "sitemap",
					);
				$this->db->insert('pages', $dataSitemap);
			}
			$this->db->set('content_page',$this->input->post('content_page'));
			$this->db->where('kategori',"sitemap");
			$this->db->update('pages');
			redirect('admin/sitemap','refresh');
		}
	}

	function checkPage($kategori){
		$this->db->where('kategori',$kategori);
		$querycheck = $this->db->get('pages')->result();
		return count($querycheck);
	}

	public function deletePengguna($id_user){
		$this->load->library('user_agent');
		// $this->db->where('id_user', $id_user);
		// $this->db->delete('pengguna');
		redirect($this->agent->referrer());
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */