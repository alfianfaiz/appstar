<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function index()
	{
		
	}

	public function login(){

		$ciphertext = md5($this->input->get('password'));
		$this->db->where('email',$this->input->get('email'));
		$this->db->where('password',$ciphertext);
		$this->db->where('verified',1);
		$query = $this->db->get('pengguna');
		foreach ($query->result() as $q) {
			$dataDB=array(array(
				'validasi' => 1 , 
				'id_user'=> $q->id_user, 
				'fullname'=> $q->fullname, 
				'username'=> $q->username, 
				'email'=> $q->email,
				'avatar' => $q->avatar,
				'address' => $q->address,
				'password' => $q->password,
			));
			// print_r($dataDB);
			echo json_encode($dataDB);
			return TRUE;
			// $this->session->set_userdata($dataDB);
		}
		$response["validasi"] = 0;
		echo json_encode(array($response));
		return FALSE;
	}

	public function coba(){
		echo $this->input->post('fullname');
	}

	public function signup(){
		

		$datamauvalidasi = array(
			'username' => $this->input->get('username'),
			'email' => $this->input->get('email'),
			);
		$this->load->library('Swiftmailer');
		$senderMail = new Swiftmailer();

		if($this->input->get('register')){
			$this->form_validation->set_data($datamauvalidasi);
			$this->form_validation->set_rules('username', 'username', 'trim|required|min_length[5]|max_length[32]|is_unique[pengguna.username]');
			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[pengguna.email]');

			if($this->form_validation->run() == FALSE) {
				// $this->register();
				// echo $this->input->get('username');
				// echo $this->input->get('fullname');
				// echo $this->input->get('email');
				// echo $this->input->get('address');
				// echo validation_errors();
				// echo "gagal neh";
				$response["pesan"] = "gagal";
				echo json_encode(array($response));
				// return true;
				return 0;
			} else {
				# code...
				// $response["pesan"] = "akejfkke";
				// echo json_encode(array($response));
				$this->load->model('pengguna');
				$model_pengguna = new Pengguna();
			

				$model_pengguna->username = $this->input->get('username');
				$model_pengguna->fullname = $this->input->get('fullname');
				$model_pengguna->city = "Semarang";
				$model_pengguna->email = $this->input->get('email');
				$model_pengguna->password = md5($this->input->get('password'));
				$model_pengguna->address = $this->input->get('address');
				$model_pengguna->level = 3;
				$model_pengguna->verify_code = $this->generateCode();
				$model_pengguna->insert_entry();

				$verify_code = $model_pengguna->verify_code;
				$link = base_url()."login/verifyme/".$model_pengguna->username;

				$response["pesan"] = "berhasil";
				echo json_encode(array($response));

				$email = $model_pengguna->email;
				$sendmail = $senderMail->sendMail("<h2>Thanks for Register</h2>$verify_code,$link","Please Confirm",$email);
				return 0;
				
			}
		} else {
			$var = "kae";
			$response["pesan"] = "gagal";
			echo json_encode(array($response));
			return 0;
		}
	}

	public function generateCode(){
		// $this->load->library('database');
		$this->load->library('RandomCode');
		$randomCode = new RandomCode;
		$RC = $randomCode->RandomPass();
		$this->db->where('verify_code',$RC);
		if($this->db->count_all_results('pengguna') == 0)
			return $RC;
		else $this->generateCode();
	}

}

/* End of file Api.php */
/* Location: ./application/controllers/Api.php */